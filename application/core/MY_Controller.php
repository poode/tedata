<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
  public function _load($template, $data = null)
	{
    // Solving gulp problem with bower componenets paths.

    // Getting the relative path by diff site_url and current_url.
    // http://127.0.0.1/tedata/
    // http://127.0.0.1/tedata/employee/add
    // to get employee/add
    $relative_path = str_replace(site_url(), '', current_url());
    // Integer 1, 2, ...
    preg_match_all('/\//', $relative_path, $match);
    $occurence = count($match[0]);
    // var_dump(	$occurence); die;

		$data["prefix"] = str_repeat('../', $occurence);

		// echo date("D - M j Y G:i:s", strtotime("2011-01-07")); die;
		$data["template"] = $template;
    // var_dump($data); die;
		// load the view file , we are passing $data array to view file.
		$this->load->ext_view('index', $data);
	}
}

?>
