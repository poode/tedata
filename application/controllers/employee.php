	<?php
	/**
	*
	* 																 / load
	* 																/
	* parentClass (CI_Controller) --> ----- view
	* 						 |								  \
	* 						 |									 \	etc...
	* 						 |
	* 						 |
	* 						 |									 / load , _load
	* 						 V									/
	* childClass (TEData_Controller) --> ----- view
	* 						 |								  \
	* 						 |									 \	etc...
	* 						 |
	* 						 |									 / load , _load
	* 						 V						      /
	* successorClass (Employee) ----> ----- view
	* 						 								    \
	* 						 										 \	etc...
	*/

class Employee extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('employee_model');
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
		if ($this->ion_auth->logged_in()){ $this->session->set_flashdata('message', 'You must login.');}
			redirect('auth/login');
		}
		if($this->ion_auth->in_group('WFM')){
			$data['emps'] = $this->employee_model->getEmps();
		}
		else{
			$data['emps'] = $this->employee_model->getEmps_leader();
		}
		$this->_load('status/afterlogin', $data);
	}

	public function logout()
	{
		$this->ion_auth->logout();
		redirect('auth/login', 'refresh');
	}


	public function view()
	{
		if (!$this->ion_auth->logged_in()) {
		if ($this->ion_auth->logged_in()){ 
			$this->session->set_flashdata('message', 'You must be an administrator.');
		}
			redirect('auth/login');
		}
		$this->employee_model->view_emps();
	}



	public function add_employee()
	{

	if (!$this->ion_auth->logged_in()) {
	if ($this->ion_auth->logged_in()){ $this->session->set_flashdata('message', 'You must be an administrator to view users data.');}

	redirect('auth/login');
	}

	$this->form_validation->set_rules('name', 'Name', 'required');
	$this->form_validation->set_rules('tedata_id', 'TEData ID', 'required');
	$this->form_validation->set_rules('ibs_id', 'IBS ID', 'required');
	$this->form_validation->set_rules('dep_id', 'Department ID (HR)', 'required');
	$this->form_validation->set_rules('title', 'Title', 'required');
	$this->form_validation->set_rules('entry_date', 'Entry Date', 'required');
	$this->form_validation->set_rules('qulification', 'Qulification', 'required');
	$this->form_validation->set_rules('notional_id', 'Notional ID', 'required');
	$this->form_validation->set_rules('address_city', 'City', 'required');
	$this->form_validation->set_rules('address_gov', 'Governament', 'required');
	$this->form_validation->set_rules('birthdate', 'Birthdate', 'required');
	$this->form_validation->set_rules('manager_id', 'Manager ID', 'required');
	$this->form_validation->set_rules('mobile', 'Mobile No.', 'required');
	$this->form_validation->set_rules('tedata_or_te_id', 'TEData Employee or TE Employee?', 'required');
	$this->form_validation->set_rules('group_id', 'Which Group this Employee Belongs to?', 'required');
	if ($this->form_validation->run() === FALSE)
	{
	$id =$this->ion_auth->user()->row()->id;
	$query = $this->db->get_where('emp', array('id =' => $id));
	$result = $query->result();
	$loggedUser = $result[0]->username;
	$whichGroupID = $result[0]->tedata_or_te_id;
	$query = $this->db->get_where('tedata_or_te', array('id =' => $whichGroupID));
	$result = $query->result();
	$whichGroup = $result[0]->type;
	$idO =$this->ion_auth->user()->row()->id;
	$nameO =$this->ion_auth->user()->row()->name;
	// print'<pre>';var_dump($nameO);print'</pre>'; die;

	if(!$this->ion_auth->is_admin()){
		$ObjectO= $this->employee_model->manager_name();
		// print'<pre>';var_dump($Object);print'</pre>'; die;

		$ObjectO[0]->tedata_id = $idO;
		$ObjectO[0]->name = $nameO;
		$length = count($ObjectO);
		for($k = $length; $k > 0; $k-- )
		{
			unset($ObjectO[$k]);

		}
					$data['managers'] = $ObjectO;
				}else{
					$data['managers'] = $this->employee_model->manager_name();
				}
				if($whichGroup==="TEData" && !$this->ion_auth->is_admin()){
					$Obj= $this->employee_model->te_or_td();
					$Obj[0]->id = "2";
					$Obj[0]->type = "TEData";
					unset($Obj[1]);
					$data['tetds'] = $Obj;
					//  print'<pre>';var_dump($Obj);print'</pre>'; die;
				}elseif($whichGroup==="TE" && !$this->ion_auth->is_admin()){
					$Obj= $this->employee_model->te_or_td();
					$Obj[0]->id = "1";
					$Obj[0]->type = "TE";
					unset($Obj[1]);
					$data['tetds'] = $Obj;
				}elseif($this->ion_auth->is_admin()){
					$data['tetds'] = $this->employee_model->te_or_td();
				}else{
					$data['tetds'] = $this->employee_model->te_or_td();
				}

				// print'<pre>';var_dump($this->employee_model->which_group());print'</pre>'; die;

				if($this->ion_auth->in_group('members') && !$this->ion_auth->is_admin()){
					$Object= $this->employee_model->which_group();
					$Object[0]->id = "3";
					$Object[0]->name = "tech";
					$length = count($Object);
					for($k = $length; $k > 0; $k-- )
					{
						unset($Object[$k]);

					}
					// print'<pre>';var_dump($Object);print'</pre>'; die;

					$data['groups'] =$Object;
				}else{
					$data['groups'] = $this->employee_model->which_group();
				}
				$this->_load('insert_employee', $data);
			}
			else
			{
				$id = $_POST['tedata_id'];
				$query=$this->db->query("SELECT name,tedata_id FROM emp WHERE tedata_id=$id");
				$result = $query->result();
				if ($result) {
					$id = $result[0]->tedata_id;
					$name = $result[0]->name;
					$data['errors'] ="This ID: $id"."is used to username ".$name;
					$this->_load('insert_employee', $data);
				}else{
					$this->employee_model->insert_employee();
					$this->_load('status/success');
				}
			}
		}

		public function add_employee_vacation()
		{
			if (!$this->ion_auth->logged_in()) {
				if ($this->ion_auth->logged_in()){ $this->session->set_flashdata('message', 'You must be an administrator to add users data.');}

				redirect('auth/login');
			}
			$this->form_validation->set_rules('vacation_id', 'Vacation Types', 'required');
			$this->form_validation->set_rules('emp_id', 'TEData ID', 'required');
			$this->form_validation->set_rules('time', 'Date and Time', 'required');
			//$this->form_validation->set_rules('userfile', 'Attachment', 'required');
			if ($this->form_validation->run() === FALSE)
			{
				$data['vacations'] = $this->employee_model->get_vacation_types();
				if($this->ion_auth->is_admin()){
					$data['emps'] = $this->employee_model->getEmps();
				}else{
					$data['emps'] = $this->employee_model->getEmps_leader();
				}
				$this->_load('vacation', $data);
			}
			else
			{
				$this->employee_model->insert_vacation();
				$this->_load('status/success_vacation');
				// }


			}

		}


		public function show_employee()
		{
			if (!$this->ion_auth->logged_in()) {
				if ($this->ion_auth->logged_in()){ $this->session->set_flashdata('message', 'You must be an administrator.');}

				redirect('auth/login');
			}
			if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group('admin')) {
				if ($this->ion_auth->logged_in()){ $this->session->set_flashdata('message', 'You must be an administrator to view users data.');}
				redirect('employee/index');
			}

			$data['emps'] = $this->employee_model->getEmps();
			$this->_load('record_view', $data);
		}

		public function delete_row($id) {
			if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group('admin')) {
				if ($this->ion_auth->logged_in()){ $this->session->set_flashdata('message', 'You must be an administrator to delete users data.');}
				redirect('employee/show');
			}
			else {
				if ($this->ion_auth->in_group('admin')){
					$this->employee_model->did_delete_row($id);
					$this->_load('status/deleted');
				}
			}

		}

		public function get_vacation()
		{
			if (!$this->ion_auth->logged_in()) {
				if ($this->ion_auth->logged_in()){ $this->session->set_flashdata('message', 'You must be an administrator to view users vacations.');}

				redirect('auth/login');
			}
			if($this->ion_auth->is_admin()){
				$data['emps'] = $this->employee_model->getEmps();
			}else{
				$data['emps'] = $this->employee_model->getEmps_leader();
			}
			$this->form_validation->set_rules('emp_id', 'TEData ID', 'required');
			$this->form_validation->set_rules('fdate', 'From Date', 'required');
			$this->form_validation->set_rules('tdate', 'To Date', 'required');
			if ($this->form_validation->run() === FALSE)
			{
				$this->_load('search_vacation',$data);
			}
			else
			{
				$data['emps_vac'] = $this->employee_model->find_vacation();

				$this->_load('status/found', $data);
			}

		}

		public function get_man_techi()
		{
			if (!$this->ion_auth->logged_in()) {
				if ($this->ion_auth->logged_in()){ $this->session->set_flashdata('message', 'You must be an administrator.');}

				redirect('auth/login');
			}
			$data['emps']=$this->employee_model->get_man_tech();
			$this->form_validation->set_rules('tedata_id', 'User', 'required');
			$this->form_validation->set_rules('manager_id', 'Manager', 'required');
			if ($this->form_validation->run() === FALSE)
			{

				$this->_load('transfare_man_tech', $data);
			} else {
			$data = $this->employee_model->transfare_tech();
			// echo "<pre>";var_dump($data); die;
			$data['done']= "manager has been changed";
			$data['emps']=$this->employee_model->get_man_tech();
			$this->_load('transfare_man_tech',$data);
			}
		}

		public  function  admin(){
			if (!$this->ion_auth->logged_in()) {
				if ($this->ion_auth->logged_in() && !$this->ion_auth->in_group('admin')){ $this->session->set_flashdata('message', 'You must be an administrator.');}

				redirect('auth/login');
			}
			$this->_load('sb-admin/pages/index');

		}

		public function att_emp(){
			if (!$this->ion_auth->logged_in()) {
				if ($this->ion_auth->logged_in()){ $this->session->set_flashdata('message', 'You must login.');}

				redirect('auth/login');
			}
			$this->_load('att');
		}

		public function checkIn(){

			if (!$this->ion_auth->logged_in()) {
				if ($this->ion_auth->logged_in()){ $this->session->set_flashdata('message', 'You must login.');}

				redirect('auth/login');
			}
			if ($this->ion_auth->in_group('admin')){
				$data['emps'] = $this->employee_model->getEmps();
			}else{
				$data['emps'] = $this->employee_model->getEmps_leader();
			}
			$this->form_validation->set_rules('emp_id', 'Check In', 'required');
			if ($this->form_validation->run() === FALSE)
			{
				$this->_load('checkin',$data);
			}else{
				$dateObj = new DateTime('now');
				$timeNow = $dateObj->format('H');
				if($timeNow <= 12) {
				$dataChecks = $this->employee_model->send_checkin();
				if(isset($dataChecks['error'])){
					$data['error'] = $dataChecks['error'];
					if ($this->ion_auth->in_group('admin')){
						$data['emps'] = $this->employee_model->getEmps();
					}else{
						$data['emps'] = $this->employee_model->getEmps_leader();
					}
					$this->_load('checkin',$data);
				}else{
					$data['emps'] = $dataChecks['emp_id'];
					$this->_load('status/checkin-suc',$data);
				}
				}else {
				$data['error'] = "Do not use console or edit URL again! operation has been reported";
				$this->_load('checkin',$data);
				}
			}


		}

		public function checkOut(){

			if (!$this->ion_auth->logged_in()) {
				if ($this->ion_auth->logged_in()){ $this->session->set_flashdata('message', 'You must login.');}
				redirect('auth/login');
			}
			if ($this->ion_auth->in_group('admin')){
				$data['emps'] = $this->employee_model->getEmps();
			}else{
				$data['emps'] = $this->employee_model->getEmps_leader();
			}
			$this->form_validation->set_rules('emp_id', 'Select an Employee', 'required');
			if ($this->form_validation->run() === FALSE)
			{
				$this->_load('checkout',$data);
			}else{
				$dateObj = new DateTime('now');
				$timeNow = $dateObj->format('H');
				if($timeNow >= 12) {
					$dataChecks	= $this->employee_model->send_checkout();
					if(isset($dataChecks['error'])){
						$data['error'] = $dataChecks['error'];
						if ($this->ion_auth->in_group('admin')){
							$data['emps'] = $this->employee_model->getEmps();
						}else{
							$data['emps'] = $this->employee_model->getEmps_leader();
						}
						$this->_load('checkout',$data);
					} else {
						$data['emps'] = $dataChecks['emp_id'];
						$this->_load('status/checkout-suc',$data);
					}
				} else {
					$data['error'] = "Do not use console or edit URL again! operation has been reported";
					$this->_load('checkout',$data);
				}
			}


		}

		public function mission_emp(){

			if (!$this->ion_auth->logged_in()) {
				if ($this->ion_auth->logged_in()){ $this->session->set_flashdata('message', 'You must login.');}

				redirect('auth/login');
			}
			if ($this->ion_auth->in_group('admin') || $this->ion_auth->in_group('WFM')){
				$data['emps'] = $this->employee_model->getEmps();
			}else{
				$data['emps'] = $this->employee_model->getEmps_leader();
			}
			$this->form_validation->set_rules('emp_id', 'Select Employee', 'required');
			$this->form_validation->set_rules('from_time', 'Select Employee', 'required');
			$this->form_validation->set_rules('to_time', 'Select Employee', 'required');
			if ($this->form_validation->run() === FALSE)
			{
				$this->_load('foundemp',$data);
			}else {
				$data['empMissions'] = $this->employee_model->get_mission();
				$this->_load('status/found-missions',$data);
			}

		}


	public function tree()
	{
		if (!$this->ion_auth->logged_in()) {
			if ($this->ion_auth->logged_in()){ $this->session->set_flashdata('message', 'You must login.');}

			redirect('auth/login');
		}
		if ($this->ion_auth->in_group('admin') || $this->ion_auth->in_group('WFM')){
			$data['emps'] = $this->employee_model->getEmps();
		}
		$this->form_validation->set_rules('manager_id', 'Manager', 'required');
		if ($this->form_validation->run() === FALSE)
		{
			$this->_load('man_tree',$data);
		} else {
			$data['EmpTree'] = $this->employee_model->getEmpByLeader();
			$id = $_POST['manager_id'];
			$query = $this->db->query("SELECT name FROM emp WHERE tedata_id = '$id'");
			$result = $query->result();
			$data['postedName'] = $result[0]->name;
			$data['postedID'] = $id;
			$this->_load('status/found_tree',$data);
		}

	}
 public function hr()
 	{ $myData = [];
		if (!$this->ion_auth->logged_in()) {
			if ($this->ion_auth->logged_in()){ $this->session->set_flashdata('message', 'You must login.');}

			redirect('auth/login');
		}
		$this->form_validation->set_rules('from_date', 'From Date', 'required');
		$this->form_validation->set_rules('to_date', 'To Date', 'required');
		if ($this->form_validation->run() === FALSE)
		{
			// echo "<pre>"; var_dump(get_class($this)); echo "</pre>";die;
			$this->_load('hr',$myData);
		} else {
			$myData['EmpsSheet'] = $this->employee_model->hrSheet();
			$this->_load('status/hr_sheet', $myData);
		}
 	}
}//end of class

	?>
