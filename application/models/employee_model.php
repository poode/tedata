			<?php
			class Employee_model extends CI_Model {

				public function __construct()
				{
					$this->load->database();
					$this->load->library('table');
					$this->load->helper('html');
					$this->load->helper('form');
					$this->load->helper('path');
					$this->load->helper('url');
					$this->load->helper('date');
					$this->load->helper('file');
					$this->load->library('form_validation');
					$config['upload_path'] = './attachments/';
					$config['allowed_types'] = 'jpg|png|pdf';
					$config['encrypt_name']=TRUE;
					$this->load->library('upload', $config);

				}


				public function view_emps()
				{
					$query = $this->db->query("SELECT * FROM emp");
					echo $this->table->generate($query);

					$query = $this->db->query("SELECT * FROM emp_mobile");
					echo $this->table->generate($query);
				}



				public function insert_employee()
				{
					$empData = array(
						'username' => $this->input->post('username'),
						'name' => $this->input->post('name'),
						'manager_id'=> $this->input->post('manager_id'),
						'tedata_id' => $this->input->post('tedata_id'),
						'ibs_id' => $this->input->post('ibs_id'),
						'title' => $this->input->post('title'),
						'qulification' => $this->input->post('qulification'),
						'entry_date' => $this->input->post('entry_date'),
						'notional_id' => $this->input->post('notional_id'),
						'email' => $this->input->post('email'),
						'address_city' => $this->input->post('address_city'),
						'address_gov' => $this->input->post('address_gov'),
						'birthdate' => $this->input->post('birthdate'),
						'landline' => $this->input->post('landline'),
						'dep_id'=> $this->input->post('dep_id'),
						'comment' => $this->input->post('comment'),
						'tedata_or_te_id' => $this->input->post('tedata_or_te_id'), //ID of TEData employee or ID for TE employee
						'id' => $this->input->post('tedata_id')
					);

					$entryDate = date_create($empData['entry_date']);
					$empData['entry_date'] = date_format($entryDate, 'Y-m-d H:i:s');

					$birthDate= date_create($empData['birthdate']);
					$empData['birthdate'] = date_format($birthDate, 'Y-m-d H:i:s');

					$this->db->insert('emp', $empData);

					$FmobileData = array(
						'emp_id' => $this->input->post('tedata_id'),
						'mobile'=> $this->input->post('mobile')
					);
					$this->db->insert('emp_mobile', $FmobileData);

					$SmobileData = array(
						'emp_id' => $this->input->post('tedata_id'),
						'mobile'=> $this->input->post('mobile2')
					);
					$this->db->insert('emp_mobile', $SmobileData);
					$groupIdData = array(
						'emp_id'=> $this->input->post('tedata_id'),
						'group_id'=> $this->input->post('group_id')
					);

					return $this->db->insert('emp_groups', $groupIdData);

				}


				public function insert_vacation()
				{
					$data['error']= ' ' ;


					$data = array(
						'vacation_id' => $this->input->post('vacation_id'),
						'emp_id'=> $this->input->post('emp_id'),
						'time' => date($this->input->post('time')),
						'comment' => $this->input->post('comment'),
						'logs' => $this->input->post('logs')
					);

					$this->upload->do_upload('attachment');
					$Path = $this->upload->data();
					$fileName=$Path['file_name'];
					$data['attachment']=$fileName;

					// print'<pre>'; var_dump($data); print'</pre>'; die;

					$timeDate= date_create($data['time']);
					$data['time'] = date_format($timeDate, 'Y-m-d H:i:s');

					return $this->db->insert('emp_vacation', $data);

				}

				public function did_delete_row($id){
					$this->db-> where( 'tedata_id',$id);
					$this->db-> delete('emp');

				}

				public function getEmps(){
					$this->db->select("id,title,mobile,comment,entry_date,qulification,notional_id,email,name,address_city,address_gov,birthdate,ibs_id,landline,dep_id,tedata_id,manager_id ");
					$this->db->from('emp');
					$this->db->join( 'emp_mobile mob', 'mob.emp_id = emp.tedata_id');
					$query = $this->db->get();
					$result = $query->result();
					$lastTedataId = 0;
					$emps = [];
					foreach ($result as $index => $emp) {
						if ($lastTedataId === $emp->tedata_id) {
							$current_mobile = $emp->mobile;
							$result[$index - 1]->mobile .= ', '. $current_mobile;
							continue;
						}

						array_push($emps, $emp);
						$lastTedataId = $emp->tedata_id;
					}

					return $emps;
				}

				public function getEmps_leader(){
					$id =$this->ion_auth->user()->row()->id;
					$this->db->select("id,title,mobile,comment,entry_date,qulification,notional_id,email,name,address_city,address_gov,birthdate,ibs_id,landline,dep_id,tedata_id,manager_id ");
					$this->db->where('manager_id',$id);
					$this->db->from('emp');
					$this->db->join( 'emp_mobile mob', 'mob.emp_id = emp.tedata_id');
					$query = $this->db->get();
					$result = $query->result();
					$lastTedataId = 0;
					$emps = [];
					foreach ($result as $index => $emp) {
						if ($lastTedataId === $emp->tedata_id) {
							$current_mobile = $emp->mobile;
							$result[$index - 1]->mobile .= ', '. $current_mobile;
							continue;
						}

						array_push($emps, $emp);
						$lastTedataId = $emp->tedata_id;
					}

					return $emps;
				}




				public function get_vacation_types()
				{
					$query = $this->db->select('id,type');
					$this->db->from('vacation');
					$query = $this->db->get();
					$data = $query->result();
					return $data;
				}

				public function users_tedata_id(){
					$this->db->select('tedata_id');
					$this->db->from('emp');
					$query = $this->db->get();
					$data = $query->result();
					return $data;
				}

				public function find_vacation()
				{
					$data = array(
						'emp_id' => $this->input->post('emp_id'),
						'fdate' => $this->input->post('fdate'),
						'tdate' => $this->input->post('tdate')
					);
					$fromDate = $data['fdate'];
					$toDate = $data['tdate'];
					$Ftime = date("Y-m-d",strtotime($fromDate));
					$Ttime = date("Y-m-d",strtotime($toDate));
					$query = $this->db->select('vacation_id,time,name,emp_id,type,emp_vacation.comment,logs,attachment');
					$this->db->from('emp_vacation');
					$this->db->where('emp_id',$data['emp_id']);
					$this->db->where("time BETWEEN '$Ftime' AND '$Ttime 23:59:59'");
					$this->db->join( 'emp', 'emp.tedata_id = emp_vacation.emp_id');
					$this->db->join('vacation','vacation.id = emp_vacation.vacation_id');
					$query = $this->db->get();
					$result = $query->result();
					$emps_vac = $result ;
					return $emps_vac;

				}

				public function te_or_td()
				{
					$query = $this->db->select('id,type');
					$this->db->from('tedata_or_te');
					$query = $this->db->get();
					$data = $query->result();
					return $data;
				}

				public function manager_name()
				{
					$query = $this->db->select('tedata_id,name');
					$this->db->from('emp');
					$query = $this->db->get();
					$data = $query->result();
					return $data;
				}

				public function which_group()
				{
					$query = $this->db->select('id,name');
					$this->db->from('groups');
					$query = $this->db->get();
					$data = $query->result();
					return $data;
				}
				public function get_man_tech(){
					$this->db->select("name,tedata_id,manager_id,group_id");
					$this->db->from('emp');
					$this->db->join('emp_groups', 'emp.tedata_id = emp_groups.emp_id');
					$query = $this->db->get();
					$data = $query->result();
					return $data;
				}
				public function transfare_tech(){
					$data = array(
						'tedata_id' => $this->input->post('tedata_id'),
						'manager_id' => $this->input->post('manager_id'),
					);
					$id = $data['tedata_id'];
					$this->db->update('emp', $data, "id = $id");
					return $data;
				}

				public function get_dep()
				{
					$query = $this->db->select('cc_id');
					$this->db->from('department');
					$query = $this->db->get();
					$data = $query->result();
					$data = $data[0]->cc_id;
					return $data;
				}

				public function send_checkin(){
					$data = array(
						'emp_id' => $this->input->post('emp_id'),
					);
					$today = mdate('%Y-%m-%d');
					$data['dep_id']=$this->get_dep();
					$data['from_time'] = mdate('%Y-%m-%d %H:%i:%s');
					$emps=$data['emp_id'];
					foreach ($emps as $emp) {
						$query=$this->db->query("SELECT id,emp_id,from_time,to_time from emp_attend
							WHERE emp_id =$emp AND from_time BETWEEN '$today' AND '$today 23:59:59'");
							$result = $query->result();
							if ($result) {
								$to_time_check = $result[0]->to_time;
								$from_time_got = $result[0]->from_time;
								$orginalFrom_time = date("Y-m-d",strtotime($from_time_got));
								$id = $result[0]->id;
								$query=$this->db->query("SELECT name FROM emp WHERE tedata_id =$emp");
								$result = $query->result();
								$username_by_id = $result[0]->name;
								$data['error'] = "This User <b>$username_by_id </b> Attended today at $from_time_got";
								return $data;
							}
							$empData = array();
							$empData['emp_id'] = $emp;
							$empData['from_time'] = $data['from_time'];
							$empData['dep_id'] = $this->get_dep();
							$this->db->insert('emp_attend', $empData);
						}
						return $data;
					}

					public function send_checkout(){
						$data = array(
							'emp_id' => $this->input->post('emp_id'),
						);
						$today = mdate('%Y-%m-%d');
						$data['dep_id']=$this->get_dep();
						$data['to_time'] = mdate('%Y-%m-%d %H:%i:%s');
						$emps=$data['emp_id'];
						foreach ($emps as $emp) {
							$query=$this->db->query("SELECT  id,from_time,to_time FROM emp_attend WHERE emp_id ='$emp' AND from_time BETWEEN '$today' and '$today 23:59:59' ORDER by from_time DESC LIMIT 1");
							$result = $query->result();
							if($result){
								$to_time_check = $result[0]->to_time;
								$from_time_got = $result[0]->from_time;
								$id = $result[0]->id;
								$orginalFrom_time = date("Y-m-d",strtotime($from_time_got));
								if ($to_time_check === NULL	&& $orginalFrom_time === $today) {
									$empData = array();
									$empData['emp_id'] = $emp;
									$empData['to_time'] = $data['to_time'];
									$empData['dep_id'] = $this->get_dep();
									$this->db->update('emp_attend', $empData, "id = $id");
									}
								} else {
									$query=$this->db->query("SELECT name FROM emp WHERE tedata_id =$emp");
									$result = $query->result();
									$username_by_id = $result[0]->name;
									$data['error'] = "There is no Check in Found or checked out before for today  for <b>$username_by_id<b/>";
									return $data;
								}
							}
							return $data;
					}



					public function get_mission(){
						$data = array(
							'emp_id' => $this->input->post('emp_id'),
							'from_time'=> $this->input->post('from_time'),
							'to_time' => $this->input->post('to_time')
						);
						$empId = $data['emp_id'];
						$fromTime = $data['from_time'];
						$fromTime = date("Y-m-d",strtotime($fromTime));
						$toTime = $data['to_time'];
						$toTime = date("Y-m-d",strtotime($toTime));
						$query=$this->db->query("SELECT emp_attend.id , name, tedata_id , from_time, to_time FROM emp_attend JOIN emp ON emp.tedata_id = emp_attend.emp_id WHERE from_time BETWEEN '$fromTime' AND '$toTime' AND emp_id = '$empId' ORDER BY from_time DESC");
						$result = $query->result();
						$empMissions =$result;
						return $empMissions;
					}

			 public function getEmpByLeader()
				 { $data=[];
					 $Pdata = array(
						 'manager_id' => $this->input->post('manager_id')
					 );
					$id = $Pdata['manager_id'];
		 			$query = $this->db->query("SELECT name,tedata_id,manager_id FROM emp WHERE manager_id = '$id' OR tedata_id = '$id'");
					 $result = $query->result();
					foreach ($result as  $key =>  $TempData) {
						$data[$key]['name'] = $TempData->name;
						$data[$key]['tedata_id'] = $TempData->tedata_id;
						$data[$key]['manager_id'] = $TempData->manager_id;
					}

		 			return $data;
				 }

			 public function hrSheet()
			   {
						$myData = array(
							'from_date' => $this->input->post('from_date'),
							'to_date' => $this->input->post('to_date')
						);
						$fromDate = $myData['from_date'];
						$toDate = $myData['to_date'];
						$Fdate = date("Y-m-d",strtotime($fromDate));
						$Tdate = date("Y-m-d",strtotime($toDate));
						$PeriodDate = date("F,Y",strtotime($Tdate));
						// print'<pre>';var_dump($PeriodDate);print'</pre>'; die;

						$query = $this->db->query("SELECT
							  emp.ibs_id,
							  emp.dep_id,
							  emp.name,
							  emp.entry_date,
								IFNULL(attendance,\"0\") AS attendance,
								IFNULL(failed,\"0\") AS failed,
								IFNULL(vacations.vacation,\"0\") AS vacation,
								IFNULL(Sicks.Sick,\"0\") AS Sick,
								IFNULL(Deductions.Deduction,\"0\") AS Deduction,
							  emp.comment
							FROM
							  emp
							LEFT JOIN
							  (
							  SELECT
							    tedata_id,
							    COUNT(*) AS attendance
							  FROM
							    emp
							  INNER JOIN
							    emp_attend ON emp.tedata_id = emp_attend.emp_id
							  WHERE
							    emp_attend.from_time BETWEEN '$Fdate' AND '$Tdate 23:59:59' AND emp_attend.to_time != 'NULL'
							  GROUP BY
							    emp.tedata_id
							) AS Att ON emp.tedata_id = Att.tedata_id
							LEFT JOIN
							  (
							  SELECT
							    emp.tedata_id,
							    COUNT(*) AS failed
							  FROM
							    emp
							  INNER JOIN
							    emp_attend ON emp.tedata_id = emp_attend.emp_id
							  WHERE
							    emp_attend.from_time BETWEEN '$Fdate' AND '$Tdate 23:59:59' AND emp_attend.to_time IS NULL
							  GROUP BY
							    emp.tedata_id
							) AS fail ON emp.tedata_id = fail.tedata_id
							LEFT JOIN
							  (
							  SELECT
							    emp_vacation.emp_id,
							    COUNT(*) AS vacation
							  FROM
							    emp_vacation
							  INNER JOIN
							    emp ON emp.tedata_id = emp_vacation.emp_id
							  INNER JOIN
							    vacation ON vacation.id = emp_vacation.vacation_id
							  WHERE
							    emp_vacation.time BETWEEN '$Fdate' AND '$Tdate 23:59:59' AND vacation.type IN(
							      'Unplanned Vacation',
							      'Planned Vacation'
							    )
							  GROUP BY
							    emp_vacation.emp_id
							) AS vacations ON emp.tedata_id = vacations.emp_id
							LEFT JOIN
							  (
							  SELECT
							    emp_vacation.emp_id,
							    COUNT(*) AS Deduction
							  FROM
							    emp_vacation
							  INNER JOIN
							    emp ON emp.tedata_id = emp_vacation.emp_id
							  INNER JOIN
							    vacation ON vacation.id = emp_vacation.vacation_id
							  WHERE
							    emp_vacation.time BETWEEN '$Fdate' AND '$Tdate 23:59:59' AND vacation.type IN('Deduction')
							  GROUP BY
							    emp_vacation.emp_id
							) AS Deductions ON emp.tedata_id = Deductions.emp_id
							LEFT JOIN
							  (
							  SELECT
							    emp_vacation.emp_id,
							    COUNT(*) AS Sick
							  FROM
							    emp_vacation
							  INNER JOIN
							    emp ON emp.tedata_id = emp_vacation.emp_id
							  INNER JOIN
							    vacation ON vacation.id = emp_vacation.vacation_id
							  WHERE
							    emp_vacation.time BETWEEN '$Fdate' AND '$Tdate 23:59:59' AND vacation.type IN('Sick Leave')
							  GROUP BY
							    emp_vacation.emp_id
							) AS Sicks ON emp.tedata_id = Sicks.emp_id
							");
				 			$result = $query->result();
							$EmpsSheet =$result;
							$EmpsSheet[0]->PeriodDate = $PeriodDate;
							return $EmpsSheet;

				 }

			}//end of class


				// print'<pre>';var_dump($data);print'</pre>'; die;

				?>
