$(document).ready(function () {
//main confirmation function for submiation actions 
  function confirmDialoug(clickedObj) {
      clickedObj.confirm({
        title: 'Confirm!',
        content: 'Are You Sure!',
        buttons: {
          confirm: {
              btnClass: 'btn-red',
              action: function () {
              $('form')[0].submit();
            }
           },
          cancel: function () {
            return;
          }
        }
      });
    }

    //// custom confirmation to delete user
  $('.delete-emp').confirm({
    title: 'Confirm!',
    content: 'Are You Sure!',
    btnClass: 'btn-danger',
    buttons: {
      confirm: {
        btnClass: 'btn-red',
        action: function () {
          window.location = this.$target.attr('data-href');
      }
     },
      cancel: function () {
        return;
      }
    }
  });

  confirmDialoug($('#getManTree'));

  if ($('#datetimepicker2')) {
    $(function () {
      $('#datetimepicker2').datetimepicker({
        // locale: 'en'
        format: 'DD-MM-YYYY'
      });
    });
  }
  if ($('#datetimepicker3')) {
    $(function () {
      $('#datetimepicker3').datetimepicker({
        // locale: 'en'
        format: 'DD-MM-YYYY'
      });
    });
  }

  if ($('#checkAll').length) {
    $('#checkAll').on('click', function () {
      $('input:checkbox').attr('checked', 'checked');
    });
  }

  if ($('#resetAll').length) {
    $('#resetAll').on('click', function () {
      $('input:checkbox').removeAttr('checked');
    });
  }

  $('#example').dataTable({
    'bLengthChange': false,
    paging: false,
    // searching: false,
    pageLength: 10000,
    dom: 'lBfrtip',
    buttons: [
      'excelHtml5', 'pdfHtml5', 'print'
    ],
    initComplete: function () {
      $('#example_length').css('float', 'left').css('margin-right', '20px');
    }
    // //  ajax: '/api/data',
    // scrollY: 200,
    // deferRender: true,
    // scroller: true
  });
  $('table.display').dataTable({
    'bLengthChange': false,
    paging: false,
    // searching: false,
    pageLength: 10000,
    dom: 'lBfrtip',
    buttons: [
      'excelHtml5', 'pdfHtml5', 'print'
    ],
    initComplete: function () {
      $('.dataTables_length').css('float', 'left').css('margin-right', '20px');
    }
    // //ajax: '/api/data',
    // scrollY: 200,
    // deferRender: true,
    // scroller: true
  });
  if ($('#confirm').length > 0) {
    confirmDialoug($('#confirm'));
  }
  if ($('#error') || $('#vali_error')) {
    $('#error').delay(10000).fadeOut();
    $('#vali_error').delay(10000).fadeOut();
  }
  // }
  // if ($('.emp').length) {
  //     var checkBoxes = $("input[name=emp_id\\[\\]]");
  //     $(".emp").click(function() {
  //         if (this.checked) {
  //             checkBoxes.prop("required", true);
  //             checkBoxes.removeAttr("required");
  //         } else {
  //             $(this).prop("required", true);
  //             for (var i = 0; i < checkBoxes.length; i++) {
  //                 if (checkBoxes[i].checked) {
  //                     checkBoxes.removeAttr("required");
  //                 }
  //             }
  //         }
  //     });
  //
  //     $('.emp')[0].setCustomValidity("Please select one employee");
  //
  // }

  //

  if ($('select[name="manager_id"]').length > 0) {
    $('select[name="manager_id"]').change(function () {
      var selectedValue = $(this).val();
      $.post({
        url: '/tedata/client/templates/ajax.php',
        data: {
          // eslint-disable-next-line camelcase
          manager_id: selectedValue
        },

        success: function (result) {
          // alert(result);
          $('select[name="tedata_or_te_id"]').val(result);
          if ($('#group_id > option').length === 2) {
            $('select[name="group_id"]').val(result === 0 ? 0 : 3);
          }
        }
      });
    });
  }

  if ($('input[name="tedata_id"]').length > 0) {
    $('#tedata_id').css('display', 'none');

    $('input[name="tedata_id"]').keyup(function () {
      var selectedValue = $(this).val();
      var lengthOf = $('input[name="tedata_id"]').val().length;
      $.post({
        url: '/tedata/client/templates/ajax.php',
        data: {
          // eslint-disable-next-line camelcase
          tedata_id: selectedValue
        },

        success: function (result) {
          //  alert(result);
          if (result && lengthOf !== 0) {
            $('#tedata_id').css('display', 'block');
            result = 'This ID reserverd for ' + result;
            $('#tedata_id').html(result);
          } else if ((!($.isNumeric(selectedValue)) && lengthOf !== 0) || ($('input[name="tedata_id"]').val().indexOf(' ') !== -1)) {
            $('#tedata_id').css('display', 'block');
            $('#tedata_id').html('Please Enter Numbers Only');
          } else {
            $('#tedata_id').css('display', 'block');
            $('#tedata_id').html('This ID is Available');
          }

          if (lengthOf === 0) {
            $('#tedata_id').css('display', 'none');
          }

          if ($('#tedata_id').text().match('This ID reserverd for') || $('#tedata_id').text().match('Please Enter Numbers Only')) {
            $('#tedata_id').removeClass('alert-info');
            $('#tedata_id').addClass('alert-danger');
          } else {
            $('#tedata_id').removeClass('alert-danger');
            $('#tedata_id').addClass('alert-info');
          }
        }
      });
    });
  }


  if ($('input[name="username"]').length > 0) {
    $('#username').css('display', 'none');

    $('input[name="username"]').keyup(function () {
      var selectedValue = $(this).val();
      var lengthOf = $('input[name="username"]').val().length;
      $.post({
        url: '/tedata/client/templates/ajax.php',
        data: {
          username: selectedValue
        },

        success: function (result) {
          //  alert(result);
          if (result && lengthOf !== 0) {
            $('#username').css('display', 'block');
            result = 'This username is used ';
            $('#username').html(result);
          } else if (($.isNumeric(selectedValue)) || lengthOf === 0 || ($('input[name="username"]').val().indexOf(' ') !== -1)) {
            $('#username').css('display', 'block');
            $('#username').html('Please Enter Letters Only');
          } else {
            $('#username').css('display', 'block');

            $('#username').html('This username is Available');
          }

          if (lengthOf === 0) {
            $('#username').css('display', 'none');
          }

          if ($('#username').text().match('This username is used') || $('#username').text().match('Please Enter Letters Only')) {
            $('#username').removeClass('alert-info');
            $('#username').addClass('alert-danger');
          } else {
            $('#username').removeClass('alert-danger');
            $('#username').addClass('alert-info');
          }
        }
      });
    });
  }
  if ($('input[name="name"]').length > 0) {
    $('#name').css('display', 'none');
    $('input[name="name"]').keyup(function () {
      var selectedValue = $(this).val();
      var lengthOf = $('input[name="name"]').val().length;
      $.post({
        url: '/tedata/client/templates/ajax.php',
        data: {
          name: selectedValue
        },

        success: function (result) {
          //  alert(result);
          if (result && lengthOf !== 0) {
            $('#name').css('display', 'block');
            result = 'This name is used ';
            $('#name').html(result);
          } else if (($.isNumeric(selectedValue)) || lengthOf === 0) {
            $('#name').css('display', 'block');
            $('#name').html('Please Enter Letters Only');
          } else {
            $('#name').css('display', 'block');

            $('#name').html('This name is Available');
          }

          if (lengthOf === 0) {
            $('#name').css('display', 'none');
          }

          if ($('#name').text().match('This name is used') || $('#name').text().match('Please Enter Letters Only')) {
            $('#name').removeClass('alert-info');
            $('#name').addClass('alert-danger');
          } else {
            $('#name').removeClass('alert-danger');
            $('#name').addClass('alert-info');
          }
        }
      });
    });
  }

  if ($('select[name="emp_id"]').length > 0) {
    $('select[name="emp_id"]').select2();
  }
  if ($('select[name="vacation_id"]').length > 0) {
    $('select[name="vacation_id"]').select2();
  }
  if ($('select[name="manager_id"]').length > 0) {
    $('select[name="manager_id"]').select2();
  }
  if ($('select[name="tedata_id"]').length > 0) {
    $('select[name="tedata_id"]').select2();
  }
  if ($('#group_id').length > 0) {
    $('#group_id').select2();
  }
  if ($('#tedataOrNot').length > 0) {
    $('#tedataOrNot').select2();
  }

  // if ($('select[name="vagroup_idcation_id"]').length > 0) {
  //   $('select[name="group_id"]').select2();
  // }
}); // end of DOM