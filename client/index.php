  <?php $url = site_url();?>
  <?php $isLogin = $this->ion_auth->logged_in(); ?>
  <?php $isAdmin = $this->ion_auth->is_admin(); ?>
  <!doctype html>
  <html lang="">
    <head>
      <meta charset="utf-8">
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <?php header("Content-Type: text/html; charset=utf-8"); ?>
      <title>TEData Attendance System</title>

      <!-- Place favicon.ico in the root directory -->
      <link rel="icon" type="image/png" href="/tedata/client/images/favicon.ico">

      <!-- build:css styles/vendor.css -->
      <!-- bower:css -->
      <link rel="stylesheet" href="<?php print $prefix;?>bower_components/bootstrap-css-only/css/bootstrap.css" />
      <link rel="stylesheet" href="<?php print $prefix;?>bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
      <link rel="stylesheet" href="<?php print $prefix;?>bower_components/datatables.net-bs/css/dataTables.bootstrap.css" />
      <link rel="stylesheet" href="<?php print $prefix;?>bower_components/datatables.net-buttons-dt/css/buttons.dataTables.css" />
      <link rel="stylesheet" href="<?php print $prefix;?>bower_components/select2/dist/css/select2.css" />
      <link rel="stylesheet" href="<?php print $prefix;?>bower_components/jquery-confirm2/css/jquery-confirm.css" />
      <!-- endbower -->
      <!-- endbuild -->

      <!-- build:css styles/main.css -->
      <link rel="stylesheet" href="<?php print $prefix;?>client/styles/main.css">
      <!-- endbuild -->

    </head>
    <body>
      <!--[if lt IE 10]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->


      <div class="container">
          <div class="text-muted right"><h3><img src="/tedata/client/images/Logo_EN.png" alt="TEData" width="150" height="50"> | <b><font color="black">A</font></b>ttendance <b><font color="black">S</font></b>ystem</h3>
          <?php if($isLogin): ?>
          <h5><b><i>Welcome <?php
          $id =$this->ion_auth->user()->row()->id;
          $query = $this->db->get_where('emp', array('id =' => $id));
          $result = $query->result();
          echo  $result[0]->username;
          ?></i></b><a class="btn btn-xs btn-primary" id='logout' href="<?php echo site_url('employee/logout');?>">Logout</a></h5></div>
          <?php endif;?>
        <div class="header">
          <ul class="nav nav-pills  topnav" id="myTopnav">

            <?php if($isLogin): ?>
              <?php if($isAdmin): ?>
              <?php $url = site_url() . 'auth'; ?>
              <li class="<?php print current_url() === $url ? 'active' : ''; ?>">
                <a href="<?php print $url?>">Users</a>
              </li>
              <?php endif;?>
              <?php if(!$isAdmin): ?>
              <?php $url = site_url() . 'employee'; ?>
              <li class="<?php print current_url() === $url ? 'active' : ''; ?>">
                <a href="<?php print $url?>">Home</a>
              </li>
            <?php endif;?>
              <?php $url = site_url() . 'employee/vacation'; ?>
              <li class="<?php print current_url() === $url ? 'active' : ''; ?>">
                <a href="<?php print $url?>">Vacation/Absence/Deduction</a>
              </li>
              <?php $url = site_url() . 'employee/search'; ?>
              <li class="<?php print current_url() === $url ? 'active' : ''; ?>">
                <a href="<?php print $url?>">Search Vacations</a>
              </li>
              <?php if($isLogin): ?>
              <?php $url = site_url() . 'employee/att'; ?>
              <li class="<?php print current_url() === $url ? 'active' : ''; ?>">
              <?php if(current_url() === site_url() . 'employee/checkIn' || current_url() === site_url() . 'employee/checkOut'): ?>
                <li class="active">
                <?php endif;?>
                <a href="<?php print $url?>">Attandance</a>
              </li>
              <?php endif;?>
              <?php if ($this->ion_auth->in_group('admin') || $this->ion_auth->in_group('WFM')):?>
              <?php $url = site_url() . 'employee/mission'; ?>
              <li class="<?php print current_url() === $url ? 'active' : ''; ?>">
                <a href="<?php print $url?>">Missions</a>
              </li>
              <?php endif;?>
              <?php if ($this->ion_auth->in_group('admin') || $this->ion_auth->in_group('WFM')):?>
              <?php $url = site_url() . 'employee/transfare'; ?>
              <li class="<?php print current_url() === $url ? 'active' : ''; ?>">
                <a href="<?php print $url?>">Change Manager</a>
              </li>
              <?php endif;?>
              <?php if ($this->ion_auth->in_group('admin') || $this->ion_auth->in_group('WFM')):?>
              <?php $url = site_url() . 'employee/tree'; ?>
              <li class="<?php print current_url() === $url ? 'active' : ''; ?>">
                <a href="<?php print $url?>">Manager Tree</a>
              </li>
              <?php endif;?>
              <?php if ($this->ion_auth->in_group('admin') || $this->ion_auth->in_group('WFM')):?>
              <?php $url = site_url() . 'employee/hr'; ?>
              <li class="<?php print current_url() === $url ? 'active' : ''; ?>">
                <a href="<?php print $url?>">HR Sheet</a>
              </li>
              <?php endif;?>

              <li class="icon">
                   <a href="javascript:void(0);" style="font-size:15px;" onclick='x = $("#myTopnav")[0]; if (x.className == "topnav") {x.className += (" responsive");
                   } else {x.className = "topnav";}'>☰</a>
              </li>
            <?php endif;?>

          </ul>

        </div>

        <div class="row marketing">
          <div class="col-lg-12">
            <?php
              include "client/templates/".$template.".php";
            ?>
          </div>
        </div>
        <div class="footer">
          <p>Copyrights for Abdul Azeem Mouneer 2017</p>
        </div>
      </div>

      <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
      <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
      </script>

      <!-- build:js scripts/vendor.js -->
      <!-- bower:js -->
      <script src="<?php print $prefix;?>bower_components/jquery/dist/jquery.js"></script>
      <script src="<?php print $prefix;?>bower_components/devbridge-autocomplete/dist/jquery.autocomplete.js"></script>
      <script src="<?php print $prefix;?>bower_components/moment/moment.js"></script>
      <script src="<?php print $prefix;?>bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
      <script src="<?php print $prefix;?>bower_components/datatables.net/js/jquery.dataTables.js"></script>
      <script src="<?php print $prefix;?>bower_components/datatables.net-bs/js/dataTables.bootstrap.js"></script>
      <script src="<?php print $prefix;?>bower_components/datatables.net-buttons/js/dataTables.buttons.js"></script>
      <script src="<?php print $prefix;?>bower_components/datatables.net-buttons/js/buttons.colVis.js"></script>
      <script src="<?php print $prefix;?>bower_components/datatables.net-buttons/js/buttons.flash.js"></script>
      <script src="<?php print $prefix;?>bower_components/datatables.net-buttons/js/buttons.html5.js"></script>
      <script src="<?php print $prefix;?>bower_components/datatables.net-buttons/js/buttons.print.js"></script>
      <script src="<?php print $prefix;?>bower_components/jszip/dist/jszip.js"></script>
      <script src="<?php print $prefix;?>bower_components/pdfmake/build/pdfmake.js"></script>
      <script src="<?php print $prefix;?>bower_components/pdfmake/build/vfs_fonts.js"></script>
      <script src="<?php print $prefix;?>bower_components/select2/dist/js/select2.js"></script>
      <script src="<?php print $prefix;?>bower_components/jquery-confirm2/js/jquery-confirm.js"></script>
      <!-- endbower -->
      <!-- endbuild -->

      <!-- build:js scripts/plugins.js -->
     
      <!-- endbuild -->

      <!-- build:js scripts/main.js -->
      <script src="<?php print $prefix;?>client/scripts/main.js"></script>
      <!-- endbuild -->
    </body>
  </html>
