  <div class="container-fluid">
    <h1 style="padding-left: 15px;" >Add User</h1>
<hr>
    <?php $errors = validation_errors();?>
    <?php if($errors):?>
      <div class="alert alert-danger">
        <?php echo $errors; ?>
      </div>
    <?php endif;?>
  <?php
    $attributes = array('class' => 'form');
    echo form_open('employee/add', $attributes);
  ?>
    <div class="col-lg-4 col-md-12 col-sm-12">
      <?php
      if ($this->ion_auth->in_group('admin')) {
        echo '<label for="title">Username to login:</label>';
        echo '<input class="form-control" type="input"  name="username" />
        <p class="alert alert-info" id="username" style="display:none;"></p><br />';
      } else {
      echo '<input class="form-control" type="hidden" name="username" value="technician"/>';
      }
      echo '<br><label for="name">Employee Name:</label>';
      echo '<input class="form-control" type="input"  name="name" />
      <div style="position:relative">
      <p style="position:absolute; padding: 5px; width:100%;" class="alert alert-info" id="name" style="display:none;"></p>
      </div>
      <br />';
      echo '<br />';
      echo '<label for="manager_id">Manager:</label>';
      echo '<select class="form-control"  name="manager_id">';
      echo '<option value="0">Please select</option>';
       foreach($managers as $manager){
      echo '<option value="'.$manager->tedata_id.'">';
      echo $manager->name;
      echo '</option>';
          }
      echo '</select>';
      echo '<br />';
      echo '<br />';
      echo '<label for="manager_id">TEData or TE Employee:</label>';
      echo '<select id= "tedataOrNot" class="form-control"  name="tedata_or_te_id">';
      echo '<option value="0">Please select</option>';
       foreach($tetds as $tetd){
      echo '<option value="'.$tetd->id.'">';
      echo $tetd->type;
      echo '</option>';
          }
      echo '</select>';
      echo '<br />';
      echo '<br />';

      echo '<label for="group_id">Which group?:</label>';
      echo '<select id="group_id" class="form-control"  name="group_id">';
      echo '<option value="0">Please select</option>';
       foreach($groups as $group){
         if(!($this->ion_auth->in_group('admin')) && $group->id == '1'){continue;}
      echo '<option value="'.$group->id.'">';
      echo $group->name;
      echo '</option>';
          }
      echo '</select>';
      echo '<br />';
      echo '<br />';
      echo '<label for="tedata_id">TEData ID:</label>';
      echo '<input class="form-control" type="input"  name="tedata_id" /><p class="alert alert-info" id="tedata_id" style="display=none;"></p><br />';
      echo '<br />';
      echo '<label for="entry_date">Entry Date:</label>';
      echo '<div class="input-group date" id="datetimepicker2">';
      echo '<input type="text" id ="entryDate" name="entry_date" class="form-control" />';
      echo '<span class="input-group-addon">';
      echo '<span class="glyphicon glyphicon-calendar"></span>';
      echo '</span>';
      echo '</div></br> ';
      echo '<br />';
      echo '<label for="birthdate">Birthdate:</label>';
      echo '<div class="input-group date" id="datetimepicker3">';
      echo '<input type="text" id="birthDate" name="birthdate" class="form-control" />';
      echo '<span class="input-group-addon">';
      echo '<span class="glyphicon glyphicon-calendar"></span>';
      echo '</span>';
      echo '</div></br> ';
      ?>
    </div>
    <div class="col-lg-4 col-md-12 col-sm-12">
      <?php
      echo '<label for="title">Employee Title:</label>';
      echo '<input class="form-control" type="input"  name="title" /><br />';
      echo '<br />';
      echo '<label for="qulification">Employee Qulification:</label>';
      echo '<input class="form-control" type="input"  name="qulification" /><br />';
      echo '<br />';
      echo '<label for="notional_id">Employee Notional ID:</label>';
      echo '<input class="form-control" type="input"  name="notional_id" /><br />';
      echo '<br />';
      echo '<label for="email">Employee Email:</label>';
      echo '<input class="form-control" type="input"  name="email" /><br />';
      echo '<br />';
      echo '<label for="address_city">City:</label>';
      echo '<input class="form-control" type="input"  name="address_city" /><br />';
      echo '<br />';
      echo '<label for="address_gov">Governament:</label>';
      echo '<input class="form-control" type="input"  name="address_gov" /><br />';
      echo '<br />';
      echo '<label for="ibs_id">IBS ID:</label>';
      echo '<input class="form-control" type="input"  name="ibs_id" /><br />';
      echo '<br />';
      ?>
    </div>
    <div class="col-lg-4 col-md-12 col-sm-12">
      <?php
      echo '<label for="landline">Landline No.</label>';
      echo '<input class="form-control" type="input"  name="landline" /><br />';
      echo '<br />';
      echo '<label for="mobile">Mobile No.</label>';
      echo '<input class="form-control" type="input"  name="mobile" /><br />';
      echo '<br />';
      echo '<label for="mobile2">Another Mobile No.</label>';
      echo '<input class="form-control" type="input"  name="mobile2" /><br />';
      echo '<br />';
      echo '<label for="dep_id">Department ID (HR):</label>';
      echo '<input class="form-control" type="input"  name="dep_id" /><br />';
      echo '<br />';
      echo '<label for="comment">Comment</label>';
      echo '<textarea class="form-control"  name="comment"></textarea><br />';
      echo '<br />';
     echo '<br />';
     echo '<input id="confirm" class="btn btn-primary" type ="submit" action="post" value="Add Employee" />';
     ?>
    </div>
  <?php  echo '</form>'; ?>
</div>
