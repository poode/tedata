   <?php if (isset($done)) {
     $str = "alert alert-success";
   } else {
     $str = "";
   }
    echo "<div id='error' class='$str'>";
    ?>
      <?php
      if(isset($done)){
        echo $done;
      }
      ?>
    </div>
    <?php $errors = validation_errors();?>
    <?php if($errors):?>
      <div id="error" class="alert alert-danger">
        <?php echo $errors; ?>
      </div>
    <?php endif;?>
      <h2>Change Manager</h2>
      <p>The form below is allowing you to change manager for recorded Users</p>

      <?php
        $attributes = array('class' => 'form');
        echo form_open('employee/transfare', $attributes);
      ?>
        <div class="input-xlarge col-sm-4">
          <label for="User">User:</label>
          <select name="tedata_id" class="form-control" >
            <option value = "">Please Select</option>
            <?php
            foreach ($emps as $emp) {
              if($emp->group_id === "3" || $emp->group_id === "2"){
                if (isset($last['tedata_id'])){
                  if($last['tedata_id'] === $emp->tedata_id && $last['name'] === $emp->name)
                    {
                      $current['tedata_id'] = $emp->tedata_id ;
                      $current['name'] = $emp->name;
                      continue;
                    }
                }
              echo "<option value=".$emp->tedata_id.">".$emp->name."</option>";
              }
              $last['tedata_id'] = $emp->tedata_id ;
              $last['name'] = $emp->name;
            }
            echo "<option value=".$current['tedata_id'].">".$current['name']."</option>";
            ?>
          </select>
          </div>
          <div class="input-xlarge col-sm-4">
            <label for="User">Manager:</label>
            <select name="manager_id" class="form-control" >
              <option value = "">Please Select</option>
              <?php
              foreach ($emps as $emp) {
                if($emp->group_id === "1" || $emp->group_id === "2"){
                  if (isset($lastm['tedata_id'])){
                    if($lastm['tedata_id'] === $emp->tedata_id && $lastm['name'] === $emp->name)
                      {
                        $currentm['tedata_id'] = $emp->tedata_id ;
                        $currentm['name'] = $emp->name;
                        continue;
                      }
                  }
                echo "<option value=".$emp->tedata_id.">".$emp->name."</option>";
                }
                $lastm['tedata_id'] = $emp->tedata_id ;
                $lastm['name'] = $emp->name;
              }
              echo "<option value=".$currentm['tedata_id'].">".$current['name']."</option>";
              ?>
              </select>
          </div>
          <div  style="padding-top: 25px;">
            <input id="confirm" class="btn btn-primary" type ="submit" action="post" value="Submit" />
          </div>

      </form>
