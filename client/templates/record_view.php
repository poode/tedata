<h4>Recorded Employees</h4>

<div class="table-responsive collapsed in" >
  <table id="example" class="table table-bordered table-striped striped " >
    <thead class="thead">
      <tr>
        <td><div class="centerBlock"><strong>Name</strong></div></td>
        <td><div class="centerBlock"><strong>Title</strong></div></td>
        <td><div class="centerBlock"><strong>TEData ID</strong></div></td>
        <?php if ($this->ion_auth->logged_in() && $this->ion_auth->in_group('admin')) {?>
        <td><div class="centerBlock"><strong>Delete from DB</strong></div></td>
        <?php }?>
      </tr>
    </thead>
    <tbody>
      <?php
      $id =$this->ion_auth->user()->row()->id;
      $query = $this->db->get_where('emp', array('id =' => $id));
      $result = $query->result();
      $loginUser= $result[0]->username;
      ?>
      <?php foreach($emps as $emp){?>
         <?php if ($emp->name == $loginUser){continue;}?>
         <tr>
          <td>
            <div class="centerBlock"><?php echo $emp->name;?></div>
          </td>
          <td>
            <div class="centerBlock"><?php echo $emp->title;?></div>
          </td>
          <td>
            <div class="centerBlock"><?php echo $emp->tedata_id;?></div>
          </td>
          <?php if ($this->ion_auth->logged_in() && $this->ion_auth->in_group('admin')) {?>
          <td>
            <div class="centerBlock">
              <a class="delete-emp btn btn-small btn-danger" data-href="<?php echo site_url('employee/delete/'.$emp->tedata_id); ?>">Delete</a>
            </div>
          </td>
          <?php }?>
          </tr>
        <?php }?>

      </tbody>
    </table>
  </div>
