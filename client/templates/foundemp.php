<?php $errors = validation_errors(); ?>
<h3>Missions done by an employer</h3>
<?php if($errors):?>
  <div class="alert alert-danger">
    <?php echo $errors; ?>
  </div>
<?php endif;?>
<hr>
<div><?php
$attributes = array('class' => 'form');
echo form_open('employee/mission', $attributes);
$loginUser= $this->ion_auth->user()->row()->username;
?>
<label for="emp_id">Select Employer: </label>
<div class="Vac-class">
<select class="form-control" id="foundemp" name="emp_id">
<option value="">Please select</option>
<?php foreach ($emps as $emp) {
   if ($emp->name == $loginUser){continue;}
  echo "<option value='$emp->tedata_id'>$emp->name</option>";
}
?>
</select>
</div>
<br>
<div class="Vac-class">
  <label for="entry_date">From Date:</label>
              <div class="input-group date" id="datetimepicker2">
                  <input type="text" id ="from_time" name="from_time" class="form-control" placeholder="From Date"/>
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
          </div>
</div>
<br>
<div class="Vac-class">
  <label for="entry_date">To Date:</label>
              <div class="input-group date" id="datetimepicker3">
                  <input type="text" id ="to_time" name="to_time" class="form-control" placeholder="To Date"/>
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
              </div>
</div>
<br>
<input id="confirm" class="btn btn-primary" type ="submit" action="post" value="Submit" />
</div>
</form>
