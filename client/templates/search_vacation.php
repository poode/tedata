<?php $errors = validation_errors();?>
<?php if($errors):?>
  <div class="alert alert-danger">
    <?php echo $errors; ?>
  </div>
<?php endif;?>

<?php $attributes = array('class' => 'form');
echo form_open('employee/search', $attributes);

echo '<div class="Vac-class">';

  echo '<label for="vacation_id">Username:</label>';
  echo '<select class="form-control" name="emp_id">';
  echo '<option value="">Please select</option>';
   foreach($emps as $emp){
  echo '<option value="'.$emp->tedata_id.'">';
  echo $emp->name;
  echo '</option>';
      }
  echo '</select>';

echo '</br><label for="entry_date">From Date:</label>';
echo '<div class="input-group date" id="datetimepicker2">';
echo '<input type="text"  name="fdate" class="form-control" placeholder="From Date"/>';
echo '<span class="input-group-addon">';
echo '<span class="glyphicon glyphicon-calendar"></span>';
echo '</span>';
echo '</div></br> ';
echo '<label for="entry_date">To Date:</label>';
echo '<div class="input-group date" id="datetimepicker3">';
echo '<input type="text"  name="tdate" class="form-control" placeholder="To Date"/>';
echo '<span class="input-group-addon">';
echo '<span class="glyphicon glyphicon-calendar"></span>';
echo '</span>';
echo '</div></br> ';
echo '</div>';
echo '<br>';
echo '<input  id="confirm" class="btn btn-primary" type ="button" value="Search" />';
echo '</form>';
?>
