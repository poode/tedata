<div class="table-responsive">
  <table  id="example" class="table table-bordered table-striped">
  <thead class="thead">
    <tr>
      <th class="text-center"><strong>IBS ID</strong></th>
      <th class="text-center"><strong>CC</strong></th>
      <th class="text-center"><strong>EMP_NAME</strong></th>
      <th class="text-center"><strong>ENTRY_DATE</strong></th>
      <th class="text-center"><strong>MISSION</strong></th>
      <th class="text-center"><strong>Failed "Check In"</strong></th>
      <th class="text-center"><strong>Annual Leave</strong></th>
      <th class="text-center"><strong>Sick Leave</strong></th>
      <th class="text-center"><strong>Deduction</strong></th>
      <th class="text-center"><strong>period</strong></th>
      <th class="text-center"><strong>comment</strong></th>
    </tr>
  </thead>
  <tbody>
    <?php
      foreach($EmpsSheet as $EmpSheet){
         echo '<tr><td class="text-center">'.$EmpSheet->ibs_id.'</td>';
         echo '<td class="text-center">'.$EmpSheet->dep_id.'</td>';
         echo '<td class="text-center">'.$EmpSheet->name.'</td>';
         echo '<td class="text-center">'.$EmpSheet->entry_date.'</td>';
         echo '<td class="text-center">'.$EmpSheet->attendance.'</td>';
         echo '<td class="text-center">'.$EmpSheet->failed.'</td>';
         echo '<td class="text-center">'.$EmpSheet->vacation.'</td>';
         echo '<td class="text-center">'.$EmpSheet->Sick.'</td>';
         echo '<td class="text-center">'.$EmpSheet->Deduction.'</td>';
         echo '<td class="text-center">'.$EmpsSheet[0]->PeriodDate.'</td>';
         echo '<td class="text-center">'.$EmpSheet->comment.'</td></tr>';
      }
   ?>
  </tbody>
  </table>
</div>
