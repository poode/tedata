<h4>The employee tree for <?php echo $postedName ?> ID: <?php echo $postedID ?></h4>
<div class="table-responsive" >
  <table id="example" class="table table-bordered table-striped striped ">
    <thead class="thead">
      <tr>
        <th><div class="centerBlock"><strong>Name</strong></div></th>
        <th><div class="centerBlock"><strong>TEData ID</strong></div></th>
      </tr>
    </thead>
    <tbody>
    
      <?php foreach($EmpTree as $emp){
        if ($emp['tedata_id']=== $postedID) continue;
        ?>
         <tr>
          <td><div class="centerBlock"><?php echo $emp['name'];?></div></td>
          <td><div class="centerBlock"><?php echo $emp['tedata_id'];?></div></td>
          </tr>
        <?php }?>
      </tbody>
    </table>
