<?php

if ($emps_vac) {
  echo '<h4>Recorded Vacations For '.$emps_vac[0]->name.'</h4>';
  echo '<div class="table-responsive">';
    echo '<table id="example" class="table table-bordered table-striped">';
      echo '<thead class="thead">';
        echo '<tr>';
          echo '<td><strong>Vacations</strong></td>';
          echo '<td><strong>Vacation date</strong></td>';
          echo '<td><strong>Comment</strong></td>';
          echo '<td><strong>Attachment</strong></td>';
          echo '<td><strong>Logs</strong></td>';
          echo '</tr>';
        echo '</thead>';
        echo '<tbody>';
         foreach($emps_vac as $emp_vac){
            echo '<tr>';
              echo '<td>'.$emp_vac->type.'</td>';
              echo '<td>'.$emp_vac->time.'</td>';
              echo '<td>'.$emp_vac->comment.'</td>';
              if(!$emp_vac->attachment) {echo '<td>No Attachment Found</td>';}
              else{
              echo '<td><a target="_blank" href="/tedata/attachments/'.$emp_vac->attachment.'" >Attachment</a></td>';
            }
              echo '<td>'.$emp_vac->logs.'</td>';
               }
              echo '</tr>';
            echo '</tbody>';
        echo '</table>';
      echo '</div>';


}
else {

echo '<h2>No Vacation Found For Requested ID</h2>';

}

?>
