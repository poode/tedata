<?php $errors = validation_errors();?>
<?php if($errors):?>
  <div id="vali_error" class="alert alert-danger">
    <?php echo $errors; ?>
  </div>
<?php endif;?>
<?php if(isset($error)):?>
  <div id="error" class="alert alert-danger">
    <?php echo $error; ?>
  </div>
<?php endif;?>
<h3>Check Out Page</h3>
<div class="table-responsive" >
  <?php  $attributes = array('class' => 'form');
    echo form_open('employee/checkOut', $attributes); ?>
  <table id="example" class="table-widthAtt table table-bordered table-striped striped ">
    <thead class="thead">
      <tr>
        <th><div class="centerBlock"><strong>Check Out</strong></div></th>
        <th><div class="centerBlock"><strong>TEData ID</strong></div></th>
      </tr>
    </thead>
    <tbody>
      <?php
      $id =$this->ion_auth->user()->row()->id;
      $query = $this->db->get_where('emp', array('id =' => $id));
      $result = $query->result();
      $loginUser= $result[0]->username;
      ?>
      <?php if($emps){ foreach($emps as $emp){?>
         <?php if ($emp->name == $loginUser){continue;}?>
         <tr>
          <td>
            <div class="centerBlock">
              <div class="checkbox">
                <label>
                  <input type="checkbox" class="emp" name="emp_id[]" value="<?php echo $emp->tedata_id;?>">&nbsp;&nbsp;<?php echo $emp->name;?>
                </label>
              </div>
            </div>
          </td>
          <td><div class="centerBlock"><?php echo $emp->tedata_id;?></div></td>
        <?php }
      }?>
      </tbody>
    </table>
    <div class="checkAtt">
      <a href="att"><button class="btn btn-info" target="_self" type="button"
        title="Click here to back."  />
        Back</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input id="checkAll" class="btn btn-primary" type ="button" value="Check All" />
    <input id="confirm" class="btn btn-primary" type ="submit" action="post" value="Submit" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input id="resetAll" class="btn btn-danger" type ="button" value="Reset" />
  </div>
  </div>
