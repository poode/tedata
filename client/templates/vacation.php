<?php $errors = validation_errors();?>
<?php $data['error'] = array('error' => $this->upload->display_errors());?>
<?php if($errors||$data['error']['error']):?>
  <div class="alert alert-danger fade in">
    <?php if($errors)echo $errors; if($data['error']['error']) echo $data['error']['error']; ?>
  </div>
<?php endif;?>


<?php $id =$this->ion_auth->user()->row()->id;
$query = $this->db->get_where('emp', array('id =' => $id));
$result = $query->result();
$usernameLogs = $result[0]->username;
$timeLogs=date('Y-m-d H:i:s A');?>

<?php $attributes = array('class' => 'form');
echo form_open_multipart('employee/vacation', $attributes);
echo '<div class="Vac-class">';
echo '<label for="vacation_id">Vacation Type:</label>';
echo '<select class="form-control" name="vacation_id">';
echo '<option value="">Please select</option>';
 foreach($vacations as $vacation){
echo '<option value="'.$vacation->id.'">';
echo $vacation->type;
echo '</option>';
    }
echo '</select>';
echo '</div>';
echo '<br>';

echo '<div class="Vac-class">';

  echo '<label for="vacation_id">Username:</label>';
  echo '<select class="form-control" name="emp_id">';
  echo '<option value="">Please select</option>';
   foreach($emps as $emp){
  echo '<option value="'.$emp->tedata_id.'">';
  echo $emp->name;
  echo '</option>';
      }
  echo '</select>';

echo '</div>';
echo '<br>';

    echo '<div class="Vac-class">';
            echo '<div class="form-group">';
                echo '<div class="input-group date" id="datetimepicker2">';
                    echo '<input type="text" name="time" class="form-control" placeholder="Enter Date"/>';
                    echo '<span class="input-group-addon">';
                        echo '<span class="glyphicon glyphicon-calendar"></span>';
                    echo '</span>';
            echo '</div></br> ';
            // var_dump(base_url()); die;
            echo '<label for="attachment">Upload Attachment:</label>';
            echo'<input name="attachment" size="20" type="file" /> </br>';

echo '<div class="Vac-class-comment">';
        echo '<label for="comment">Comment:</label>';
        echo '<textarea id="textBlank" class="form-control" name="comment" ></textarea></br>';
        // $logs = array('logs' => 'added by '.$usernameLogs.' at '.$timeLogs.'');
        // $_POST['logs'] = $logs;
        echo '<textarea id="hiddedText" class="unSeen" name="logs" >added by '.$usernameLogs.' at '.$timeLogs.'</textarea>';
        echo '<input id="confirm" class="btn btn-primary" type ="submit" action ="/employee/add_employee_vacation" method="post" value="Commit Vacation Type" />';



echo '</div>';
echo '</form>';

?>
