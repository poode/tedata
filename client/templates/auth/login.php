<h1><?php echo lang('login_heading');?></h1>
<p><?php echo lang('login_subheading');?></p>

<?php if($message):?>
  <div id="infoMessage" class="alert alert-danger fade in"><?php echo $message;?></div>
<?php endif?>

<?php
$attributes = array('class' => 'form col-lg-6');
echo form_open("auth/login", $attributes);
?>

  <p>
    <?php echo lang('login_identity_label', 'identity');?>
    <?php
      $identity['class'] = 'form-control';
      echo form_input($identity);
    ?>
  </p>
  <p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>
  <p>
    <?php echo lang('login_password_label', 'password');?>
    <?php
      $password['class'] = 'form-control';
      echo form_input($password);
    ?>
  </p>

  <p>
    <?php echo lang('login_remember_label', 'remember');?>
    <?php
      echo form_checkbox('remember', '1', FALSE, 'id="remember"');
    ?>
  </p>


  <p><?php
    $submit = array('class' => 'btn btn-primary' , 'name' => 'submit');
    echo form_submit($submit, lang('login_submit_btn'));
  ?></p>

<?php echo form_close();?>
