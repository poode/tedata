<?php $url = site_url() . 'employee/add'; ?>
<a class="btn btn-primary pull-right" href="<?php print $url?>">Add Employee</a>
<h1><?php echo lang('index_heading');?></h1>
<p><?php echo lang('index_subheading'); echo " Managment"?></p>
<?php if($message):?>
  <div id="infoMessage" class="alert alert-danger fade in"><?php echo $message;?></div>
<?php endif?>

<table id=""  class="display table table-bordered table-striped striped ">
<thead class="thead">
	<tr>
		<!-- <th><?//php echo lang('index_fname_th');?></th> -->
		<!-- <th><?//php echo lang('index_lname_th');?></th> -->
		<th><?php echo "Username";?></th>
		<th><?php echo lang('index_groups_th');?></th>
		<th><?php echo lang('index_status_th');?></th>
		<th><?php echo lang('index_action_th');?></th>
	</tr>
	</thead>
	  <tbody>

	<?php foreach ($emps as $emp):?>
            <!-- <td><?//php echo htmlspecialchars($emp->first_name,ENT_QUOTES,'UTF-8');?></td> -->
            <!-- <td><?//php echo htmlspecialchars($emp->last_name,ENT_QUOTES,'UTF-8');?></td> -->
						<?php  //echo "<pre>";var_dump($emp);
             if ($emp->groups[0]->name != "tech"){
						 echo "<tr>";
            ?>
            <td><?php

							 echo htmlspecialchars($emp->username,ENT_QUOTES,'UTF-8');
						 ?></td>
			<td>
				<?php foreach ($emp->groups as $group):?>
					<?php
						echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;
					?><br />
                <?php endforeach?>
			</td>
			<td><?php
			echo ($emp->active) ? anchor("auth/deactivate/".$emp->id, lang('index_active_link')) : anchor("auth/activate/". $emp->id, lang('index_inactive_link')); ?></td>
			<td><?php
			 echo anchor("auth/edit_user/".$emp->id, 'Edit');  echo "</tr>";

		 }	?></td>

	<?php endforeach;?>
	<?php  ?>
</tbody>
</table>
<p><?php echo lang('index_subheading'); echo " Technician"?></p>

<table id="" class="display table table-bordered table-striped striped ">
<thead class="thead">
	<tr>
		<!-- <th><?//php echo lang('index_fname_th');?></th> -->
		<!-- <th><?//php echo lang('index_lname_th');?></th> -->
		<th><?php echo "Username";?></th>
		<th><?php echo lang('index_groups_th');?></th>
		<th><?php echo lang('index_status_th');?></th>
		<th><?php echo lang('index_action_th');?></th>
	</tr>
	</thead>
	  <tbody>

	<?php foreach ($emps as $emp):?>

		<?php

		// var_dump($emp->groups[0]->name ); die; ?>


            <!-- <td><?//php echo htmlspecialchars($emp->first_name,ENT_QUOTES,'UTF-8');?></td> -->
            <!-- <td><?//php echo htmlspecialchars($emp->last_name,ENT_QUOTES,'UTF-8');?></td> -->
						<?php if ($emp->groups[0]->name == "tech"){
						 echo "<tr>"; ?>
            <td><?php

							 echo htmlspecialchars($emp->name,ENT_QUOTES,'UTF-8');
						 ?></td>
			<td>
				<?php foreach ($emp->groups as $group):?>
					<?php
						echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;
					?><br />
                <?php endforeach?>
			</td>
			<td><?php
			echo ($emp->active) ? anchor("auth/deactivate/".$emp->id, lang('index_active_link')) : anchor("auth/activate/". $emp->id, lang('index_inactive_link')); ?></td>
			<td><?php
			 echo anchor("auth/edit_user/".$emp->id, 'Edit');  echo "</tr>";

		 }	?></td>

	<?php endforeach;?>
	<?php  ?>
</tbody>
</table>
</div>
<p><?php echo anchor('employee/add', lang('index_create_user_link'))?> | <?php echo anchor('auth/create_group', lang('index_create_group_link'))?> | <?php echo anchor('employee/show' ,lang('index_delete_emp')) ?></p>
