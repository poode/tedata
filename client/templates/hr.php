<?php $errors = validation_errors();?>
<?php if($errors):?>
  <div id="vali_error" class="alert alert-danger">
    <?php echo $errors; ?>
  </div>
<?php endif;?>
<?php
$attributes = array('class' => 'form');
echo form_open('employee/hr', $attributes);
?>
<div class="Vac-class">
  <label for="entry_date">From Date:</label>
              <div class="input-group date" id="datetimepicker2">
                  <input type="text" id ="from_date" name="from_date" class="form-control" placeholder="From Date"/>
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
          </div>
</div>
<br>
<div class="Vac-class">
  <label for="entry_date">To Date:</label>
              <div class="input-group date" id="datetimepicker3">
                  <input type="text" id ="to_date" name="to_date" class="form-control" placeholder="To Date"/>
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
              </div>
</div>
<br>
<input id="confirm" class="btn btn-primary" type ="submit" action="post" value="Submit" />
</form>
