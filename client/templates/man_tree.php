<?php $errors = validation_errors();?>
<?php if($errors):?>
  <div id="vali_error" class="alert alert-danger">
    <?php echo $errors; ?>
  </div>
<?php endif;?>
<h3>Manager Tree</h3>
<p>Manager and his Employee</p>
<?php
  $attributes = array('class' => 'form', 'method' =>'post');
  echo form_open('employee/tree', $attributes);
?>
  <div class="input-xlarge col-sm-4">
    <label style="margin-bottom: 10px;" for="User">User:</label>
    <br/>
    <select name="manager_id" class="form-control" >
      <option value = "">Please Select</option>
      <?php
        foreach ($emps as $emp)
          {
            echo "<option value=".$emp->tedata_id.">".$emp->name."</option>";
          }
      ?>
      </select>
</div>

  <input style="margin-top: 78px; margin-left: -357px;" id="getManTree" class="btn btn-primary" type ="submit" value="Submit" />

</form>
