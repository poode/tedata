-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: tedata
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `cc_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_gov` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_area` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1372 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1371,'cairo','east cairo');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp`
--

DROP TABLE IF EXISTS `emp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emp` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'tech',
  `comment` varchar(160) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_date` date NOT NULL DEFAULT '2000-01-01',
  `qulification` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  `notional_id` bigint(30) NOT NULL DEFAULT '0',
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'tech@tedata.net',
  `name` varchar(160) COLLATE utf8_unicode_ci NOT NULL,
  `address_city` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `address_gov` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `birthdate` date NOT NULL DEFAULT '2000-01-02',
  `ibs_id` int(15) NOT NULL DEFAULT '0',
  `landline` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `dep_id` int(11) NOT NULL,
  `tedata_id` int(15) NOT NULL,
  `tedata_or_te_id` int(11) NOT NULL,
  `manager_id` int(20) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT '127.0.0.1',
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activation_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forgotten_password_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` int(11) unsigned DEFAULT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`,`tedata_id`),
  UNIQUE KEY `my_unique_key` (`tedata_id`),
  KEY `dep_id` (`dep_id`),
  KEY `manager_id` (`manager_id`),
  KEY `tedata_or_te_id` (`tedata_or_te_id`),
  KEY `ibs_id` (`ibs_id`),
  CONSTRAINT `emp_ibfk_1` FOREIGN KEY (`tedata_or_te_id`) REFERENCES `tedata_or_te` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=123455545 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp`
--

LOCK TABLES `emp` WRITE;
/*!40000 ALTER TABLE `emp` DISABLE KEYS */;
INSERT INTO `emp` VALUES (951,'Supervisior','','2017-01-25','Engineering BSC',2560412310236,'eng.abdul.azeem@gmail.com','Abdelazeem.mueer','Cairo','Cairo','1987-12-05',0,'0224993706',1371,951,2,2356,'127.0.0.1','Abdul-Azeem Mohammed','$2y$08$OkW7tt0o.BTd/HQ2eFxRk.nXAFbFAHngF2okbLbdCgoa/ZEYAmwJ.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(952,'tech','','2017-01-18','none',1234567895421,'yyy@gmail.com','Mohamed Ali','Cairo','Cairo','2017-01-26',123545,'02-36892156',1371,952,2,5175,'127.0.0.1','mohammed','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2356,'unit','no comment','2007-10-09','BSC',2560123010236,'ashraf@gmail.com','admin','Cairo','Cairo','1972-07-20',123545,'0224993706',1371,2356,2,23165,'127.0.0.1','admin','$2y$08$SsaKOOwozErJuUSCB6kCnuUue6tf6nud8/yN4b8PoVRhkE9L2Rx5y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(5051,'tech','','2017-01-11','none',2560123010236,'eng.abdul.azeem@gmail.com','ahmed','Cairo','Cairo','1997-07-24',123546,'0224993706',1371,5051,2,50504,'127.0.0.1','ahmed','$2y$08$qAQWy0mJW9lpNWUJsAd/cOkY75cXXSq.yivXZyEdmbj1p.j4lB526',NULL,NULL,NULL,NULL,NULL,NULL,1485020471,1),(5175,'Supervisior','Since 2003','2003-10-29','Engineering BSC',2560123010236,'eng.abdul.azeem@gmail.com','Tarek.Eldsoky','Cairo','Cairo','1975-08-13',0,'02-36892156',1371,5175,2,0,'127.0.0.1','Tarek.Eldsoky','$2y$08$ZBJaOY.hyHbRLI9Im7D7..ZQ9fSFzynmL43/ms0NbkB5dc75e9z.K',NULL,NULL,NULL,NULL,'tKuZ/IOLxUaIGojk9QIPzu',NULL,1486228610,1),(12345,'tech','','2017-02-20','none',2560123010777,'xyz@xyz.com','test25','Cairo','Cairo','2000-12-29',652987,'0224993706',1371,12345,2,5175,'127.0.0.1','technician','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23165,'tech','','2016-12-01','Engineering BSC',1234567895421,'eng.abdul.azeem@gmail.com','mahmoud','Cairo','Cairo','2000-07-31',56231,'0224993706',1371,23165,2,5175,'127.0.0.1','mahmoud','$2y$08$mfHXuLBjzVk1pR3CGrC7Q.fs/N7HYjZ29Uh7MITBw/PkhoegNRYQi',NULL,NULL,NULL,NULL,NULL,NULL,1487631059,1),(44477,'Supervisior','','2006-12-02','Engineering BSC',1234567895421,'Eng.Abdul.azeem@gmail.com','abdul','NEW YORK','Cairo','2019-12-02',0,'17185538740',1371,44477,2,952,'127.0.0.1','abdul','$2y$08$IVSPqfV.O7SdBjLWc8KNNOgQ/CsSVSSJsbE7.YXxdrxrOCqfU1Ila',NULL,'dbda631384208bb35713867c513d7a3a819cd041',NULL,NULL,NULL,0,NULL,0),(50504,'فني','','2017-01-27','none',2560123010236,'newuser2@gmail.com','newuser2','Cairo','Cairo','1999-01-02',564984,'02-36892156',1371,50504,1,4563211,'127.0.0.1','newuser2','$2y$08$C4AWwqol7IaM/MDTjQMmSu1yIBDFU/.Ur0uTgwoRDe2Q9UdokiF6W',NULL,NULL,NULL,NULL,NULL,NULL,1485020688,1),(50545,'tech','','2017-01-19','none',1234567895421,'eng.abdul.azeem@gmail.com','Abdul-Azeem Mohammed','Cairo','Cairo','2017-02-10',1235454,'0224993706',1371,50545,2,23165,'127.0.0.1','Abdul-Azeem Mohammed','$2y$08$thk4JJAGGOGgUiYpIpvkju2JhObhDBQp0vuB1Hhqhj0A6Q5PNF4fG',NULL,NULL,NULL,NULL,NULL,NULL,1485351487,1),(565789,'WFM S','another test after drop down all','2016-12-01','Engineering BSC',1234567895421,'wfm@gmail.com','another.test','Cairo','Cairo','2016-12-29',123545,'02-36892156',1371,565789,1,1234557,'127.0.0.1','another.test','$2y$08$1TxuTVNic7FneKcUkOleMeQ0woVObM1DIUsMxABmOXDInoMFOMxmC',NULL,NULL,NULL,NULL,NULL,NULL,1487628704,1),(1234555,'Supervisior','','2010-12-02','Engineering BSC',2560123010236,'eng.abdul.azeem@gmail.com','poode','NEW YORK','Cairo','2019-12-02',1235,'0224993706',1371,1234555,2,0,'127.0.0.1','poode','$2y$08$Us2B.9VpQPfgWBJl6bz/deaONZs74qj4EzFhOU.9SR6n8ohyOYPeC',NULL,NULL,'m6oEKCP.bBzc9.UEPydUHuab86b03556dc8f50b8',1480212958,'/0gU7iZzznFLbgqUP8mlou',0,1487782648,1),(1234557,'tech','','2010-12-02','none',1234567895421,'Eng.Abdul.azeem@gmail.com','poode11','NEW YORK','Cairo','2019-12-02',0,'17185538740',1371,1234557,2,0,'127.0.0.1','poode11','$2y$08$RRlkhEedrLDpbNsDig7HSOEtZmYS45WDjhZS8fqA2DxEEVBNEVemy',NULL,NULL,NULL,NULL,'D/wW7fpJto/PZpmnZt3SBO',0,1482239539,1),(4563211,'techncian','','2017-01-01','BSC',2560412310236,'newuser@tedata.net','NewUser','Cairo','Cairo','1983-02-02',2365,'0224993706',1371,4563211,1,565789,'127.0.0.1','NewUser','$2y$08$/NMFoFOSv7.2uTTqzdm4YuFIpx9mMoIm0mS4She8UXgW2Y/fqkCOC',NULL,NULL,NULL,NULL,NULL,NULL,1485009643,1),(123455544,'Supervisior','','2006-12-02','Engineering BSC',1234567895421,'eng.abdul.azeem@gmail.com','pooe','Ain Shams','Cairo','2019-12-02',1235,'17185538740',1371,123455544,2,0,'127.0.0.1','pooe','$2y$08$hNvksA6M3jm39Y50wns6xOLA1wupL30iTSAGuuy8F27EcTyy6BW2C',NULL,NULL,NULL,NULL,NULL,0,1481697142,1);
/*!40000 ALTER TABLE `emp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp_attend`
--

DROP TABLE IF EXISTS `emp_attend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emp_attend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `dep_id` int(11) NOT NULL,
  `from_time` datetime NOT NULL,
  `to_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`),
  KEY `dep_id` (`dep_id`),
  CONSTRAINT `emp_attend_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp` (`tedata_id`),
  CONSTRAINT `emp_attend_ibfk_2` FOREIGN KEY (`dep_id`) REFERENCES `department` (`cc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp_attend`
--

LOCK TABLES `emp_attend` WRITE;
/*!40000 ALTER TABLE `emp_attend` DISABLE KEYS */;
INSERT INTO `emp_attend` VALUES (1,44477,1371,'2017-01-01 08:30:51','2017-01-01 17:17:36'),(2,44477,1371,'2017-01-25 15:30:51','2017-01-26 01:13:11'),(3,44477,1371,'2017-01-25 15:30:51','2017-01-26 01:13:11'),(4,44477,1371,'2017-01-25 15:30:51',NULL),(5,4563211,1371,'2017-01-26 01:38:15','2017-01-26 01:38:43'),(6,951,1371,'2017-01-26 01:43:49','2017-01-26 01:44:15'),(7,1234557,1371,'2017-01-26 01:52:26','2017-01-26 01:52:40'),(8,2356,1371,'2017-01-26 02:03:14','2017-01-26 02:21:52'),(9,5051,1371,'2017-01-26 02:20:19','2017-01-26 02:21:52'),(10,50545,1371,'2017-01-25 15:30:51',NULL),(11,565789,1371,'2017-01-26 02:20:19',NULL),(12,23165,1371,'2017-01-26 02:20:19',NULL),(13,50504,1371,'2017-01-26 01:38:15',NULL),(14,123455544,1371,'2017-01-26 02:24:31',NULL),(16,952,1371,'2017-01-26 02:20:19',NULL),(17,5175,1371,'2017-01-26 02:24:31',NULL),(29,951,1371,'2017-01-28 19:10:17','2017-01-28 19:19:18'),(30,44477,1371,'2017-01-28 19:10:17','2017-01-28 19:19:18'),(31,50545,1371,'2017-01-28 19:10:17','2017-01-28 19:19:18'),(32,2356,1371,'2017-01-28 19:10:17','2017-01-28 19:19:18'),(34,565789,1371,'2017-01-28 19:10:17','2017-01-28 19:19:18'),(35,23165,1371,'2017-01-28 19:10:17','2017-01-28 19:19:18'),(36,952,1371,'2017-01-28 19:10:17','2017-01-28 19:19:18'),(37,4563211,1371,'2017-01-28 19:10:17','2017-01-28 19:19:18'),(39,1234557,1371,'2017-01-28 19:10:17','2017-01-28 19:19:18'),(41,5175,1371,'2017-01-28 19:10:17','2017-01-28 19:19:18'),(42,44477,1371,'2017-01-28 19:53:21',NULL),(43,952,1371,'2017-01-28 19:54:50',NULL),(44,565789,1371,'2017-01-28 19:56:45',NULL),(45,951,1371,'2017-01-28 20:17:30',NULL),(65,50504,1371,'2017-01-28 22:39:36',NULL),(67,5051,1371,'2017-01-28 22:42:39',NULL),(68,44477,1371,'2017-01-29 00:23:34',NULL),(69,50545,1371,'2017-01-29 00:23:34',NULL),(70,2356,1371,'2017-01-29 00:23:34',NULL),(71,4563211,1371,'2017-01-29 00:36:10',NULL),(75,5051,1371,'2017-01-31 22:36:34','2017-01-31 22:39:48'),(76,50545,1371,'2017-02-04 01:02:54',NULL),(77,951,1371,'2017-02-04 01:17:05','2017-02-04 13:56:39'),(78,951,1371,'2017-02-05 08:37:32',NULL);
/*!40000 ALTER TABLE `emp_attend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp_groups`
--

DROP TABLE IF EXISTS `emp_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emp_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `emp_id` int(15) NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`emp_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`emp_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp_groups`
--

LOCK TABLES `emp_groups` WRITE;
/*!40000 ALTER TABLE `emp_groups` DISABLE KEYS */;
INSERT INTO `emp_groups` VALUES (49,951,1),(50,952,2),(18,2356,1),(10,5050,3),(42,5051,2),(54,5175,2),(55,5175,4),(56,5175,5),(57,12345,3),(24,23165,2),(31,44477,3),(44,50504,2),(46,50545,2),(47,50545,3),(20,565789,4),(58,1234555,1),(4,1234557,1),(40,4563211,2),(19,123455544,2);
/*!40000 ALTER TABLE `emp_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp_mobile`
--

DROP TABLE IF EXISTS `emp_mobile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emp_mobile` (
  `emp_id` int(11) NOT NULL,
  `mobile` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `emp_id` (`emp_id`),
  CONSTRAINT `emp_id_fk_mobile` FOREIGN KEY (`emp_id`) REFERENCES `emp` (`tedata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp_mobile`
--

LOCK TABLES `emp_mobile` WRITE;
/*!40000 ALTER TABLE `emp_mobile` DISABLE KEYS */;
INSERT INTO `emp_mobile` VALUES (1234557,'01004395287'),(1234555,'01004395287'),(123455544,'01004395287'),(44477,'17185538740'),(44477,'01004395287'),(5175,'01004395287'),(5175,'01113043809'),(565789,'01254257575'),(565789,'01004395287'),(2356,'01004395287'),(2356,'01279180217'),(23165,'01004395287'),(23165,'01279180217'),(4563211,'01004395287'),(4563211,'01279180217'),(50504,'01113043809'),(50504,'01279180217'),(5051,'01113043809'),(5051,'01279180217'),(50545,'01004395287'),(50545,'01279180217'),(951,'01113043809'),(951,'01279180217'),(952,'01004395287'),(952,'01113043809'),(12345,'01113043809'),(12345,'01004395287');
/*!40000 ALTER TABLE `emp_mobile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp_vacation`
--

DROP TABLE IF EXISTS `emp_vacation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emp_vacation` (
  `emp_id` int(11) NOT NULL,
  `vacation_id` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `comment` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logs` varchar(260) COLLATE utf8_unicode_ci NOT NULL,
  KEY `emp_id` (`emp_id`),
  KEY `vacation_id` (`vacation_id`),
  CONSTRAINT `emp_vacation_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp` (`tedata_id`),
  CONSTRAINT `emp_vacation_ibfk_2` FOREIGN KEY (`vacation_id`) REFERENCES `vacation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp_vacation`
--

LOCK TABLES `emp_vacation` WRITE;
/*!40000 ALTER TABLE `emp_vacation` DISABLE KEYS */;
INSERT INTO `emp_vacation` VALUES (1234555,3,'2016-11-14 03:52:07','',NULL,''),(1234557,3,'2016-11-14 04:08:01','',NULL,''),(1234555,2,'2016-11-16 00:09:30','',NULL,''),(1234555,3,'2016-11-19 15:19:05','',NULL,''),(44477,2,'2016-11-20 11:35:49','',NULL,''),(123455544,1,'2016-11-21 02:05:17','',NULL,''),(1234557,3,'2016-11-21 02:08:04','',NULL,''),(1234557,3,'2016-11-21 02:09:11','',NULL,''),(44477,4,'2016-11-21 03:32:23','',NULL,''),(1234555,1,'2016-11-21 03:33:21','',NULL,''),(44477,1,'2016-11-08 00:00:00','',NULL,''),(44477,1,'2016-12-07 00:00:00','test',NULL,''),(1234555,2,'2016-12-06 00:00:00','yyyy',NULL,''),(5175,1,'2016-12-29 00:00:00','no words',NULL,'Tarek at 2016-12-08 01:06:17 AM'),(2356,2,'2016-12-20 00:00:00','',NULL,'added by pooe at 2016-12-14 09:59:34 AM'),(5175,1,'2016-12-26 00:00:00','',NULL,'added by pooe at 2016-12-14 10:02:39 AM'),(44477,2,'2016-12-20 00:00:00','44477',NULL,'added by pooe at 2016-12-14 10:15:26 AM'),(44477,2,'2016-12-20 00:00:00','44477',NULL,'added by pooe at 2016-12-14 10:19:46 AM'),(5175,2,'2016-12-20 00:00:00','44477',NULL,'added by pooe at 2016-12-14 10:20:19 AM'),(565789,2,'2016-12-20 00:00:00','44477',NULL,'added by pooe at 2016-12-14 10:24:11 AM'),(565789,1,'2016-12-28 00:00:00','',NULL,'added by pooe at 2016-12-14 10:25:29 AM'),(2356,2,'2016-12-26 00:00:00','',NULL,'0'),(565789,4,'2016-12-13 00:00:00','',NULL,'0'),(5175,3,'2016-12-26 00:00:00','',NULL,'added by pooe at 2016-12-14 11:09:30 AM'),(5175,1,'2017-01-04 00:00:00','',NULL,'added by pooe at 2016-12-14 11:49:22 AM'),(5175,2,'2017-01-11 00:00:00','',NULL,'added by poode at 2017-01-06 21:45:42 PM'),(5175,2,'2017-01-11 00:00:00','',NULL,'added by poode at 2017-01-06 21:45:42 PM'),(5175,2,'2017-01-11 00:00:00','',NULL,'added by poode at 2017-01-06 21:45:42 PM'),(23165,2,'2017-01-31 00:00:00','','LTE.png','added by poode at 2017-01-06 22:00:23 PM'),(5175,1,'2017-01-10 00:00:00','',NULL,'added by poode at 2017-01-07 15:17:08 PM'),(2356,2,'2017-01-25 00:00:00','',NULL,'added by poode at 2017-01-07 20:13:08 PM'),(1234555,4,'2017-01-30 00:00:00','',NULL,'added by poode at 2017-01-07 20:19:41 PM'),(2356,2,'2017-01-01 00:00:00','',NULL,'added by poode at 2017-01-07 21:17:54 PM'),(123455544,2,'2017-02-09 00:00:00','',NULL,'added by poode at 2017-01-07 21:18:20 PM'),(5175,3,'2017-02-01 00:00:00','',NULL,'added by poode at 2017-01-07 21:22:37 PM'),(5175,4,'2017-01-31 00:00:00','',NULL,'added by poode at 2017-01-07 21:23:03 PM'),(23165,6,'2017-01-24 00:00:00','',NULL,'added by poode at 2017-01-07 21:23:49 PM'),(44477,2,'2017-02-11 00:00:00','',NULL,'added by mahmoud at 2017-01-07 21:26:45 PM'),(5175,4,'2017-03-30 00:00:00','',NULL,'added by poode at 2017-01-07 21:41:41 PM'),(565789,3,'2017-06-01 00:00:00','','0','added by poode at 2017-01-07 21:44:16 PM'),(565789,3,'2017-02-01 00:00:00','',NULL,'added by poode at 2017-01-07 21:54:36 PM'),(23165,3,'2017-01-28 00:00:00','',NULL,'added by poode at 2017-01-07 22:09:27 PM'),(23165,3,'2017-01-26 00:00:00','',NULL,'added by poode at 2017-01-07 22:12:04 PM'),(23165,3,'2017-02-09 00:00:00','',NULL,'added by poode at 2017-01-07 22:12:59 PM'),(2356,2,'2017-02-09 00:00:00','',NULL,'added by poode at 2017-01-07 22:17:53 PM'),(5175,2,'2017-02-08 00:00:00','','','added by poode at 2017-01-07 22:20:08 PM'),(565789,1,'2017-01-26 00:00:00','','','added by poode at 2017-01-14 13:59:48 PM'),(5175,3,'2017-01-30 00:00:00','','','added by poode at 2017-01-14 14:01:38 PM'),(23165,2,'2017-01-31 00:00:00','','','added by poode at 2017-01-14 14:04:16 PM'),(1234555,5,'2017-01-31 00:00:00','','','added by poode at 2017-01-14 14:06:01 PM'),(44477,3,'2017-01-31 00:00:00','','30f0a803ad0c17a78e0b1200134d7894.pdf','added by poode at 2017-01-14 14:38:47 PM'),(2356,3,'2017-01-24 00:00:00','','ece719dd904e9fff05213d4a819e5c03.png','added by mahmoud at 2017-01-14 14:53:16 PM'),(1234557,7,'2017-01-21 00:00:00','تعامل بأسلوب غير لائق مع العميل','','added by another.test at 2017-01-21 16:15:01 PM'),(44477,7,'2017-01-27 00:00:00','خصم يوم كامل نتيجة الأهمال','','added by poode at 2017-01-27 21:48:32 PM'),(951,1,'2017-01-25 00:00:00','','ee70cdad4c79d51d78b62d17d77f2819.jpg','added by poode at 2017-01-29 00:18:57 AM'),(12345,4,'2017-02-28 00:00:00','Test','20164cee7e613fd73df5a7317e7360be.png','added by poode at 2017-02-04 01:55:28 AM'),(12345,4,'2017-02-28 00:00:00','Test','37f6da5398b003ace95625ebd808335e.png','added by poode at 2017-02-04 01:55:28 AM'),(5175,7,'2017-02-20 00:00:00','','','added by poode at 2017-02-20 21:58:05 PM'),(5175,7,'2017-02-20 00:00:00','','','added by poode at 2017-02-20 21:58:05 PM'),(50545,1,'2017-02-21 00:00:00','test feb 21','8313a4419e6a7878c85b3d67e2f0d5b8.jpg','added by mahmoud at 2017-02-21 01:27:50 AM'),(5175,1,'2017-02-22 00:00:00','[removed] document.alert&#40;\'hello\'&#41; [removed]','','added by poode at 2017-02-22 11:29:31 AM');
/*!40000 ALTER TABLE `emp_vacation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `firestick_log_template`
--

DROP TABLE IF EXISTS `firestick_log_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `firestick_log_template` (
  `ip` int(11) NOT NULL,
  `page` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `referrer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logged` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `memory` int(10) unsigned NOT NULL,
  `render_elapsed` float NOT NULL,
  `ci_elapsed` float NOT NULL,
  `controller_elapsed` float NOT NULL,
  `mysql_elapsed` float NOT NULL,
  `mysql_count_queries` tinyint(3) unsigned NOT NULL,
  `mysql_queries` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=ARCHIVE DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `firestick_log_template`
--

LOCK TABLES `firestick_log_template` WRITE;
/*!40000 ALTER TABLE `firestick_log_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `firestick_log_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'admin','Administrator'),(2,'members','General User'),(3,'tech','technician '),(4,'WFM','Create users, active and de-active users, view all users '),(5,'Supervisor','');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_attempts`
--

LOCK TABLES `login_attempts` WRITE;
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mission`
--

DROP TABLE IF EXISTS `mission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `address_gov` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address_details` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `emp_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`),
  CONSTRAINT `Mission_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp` (`tedata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mission`
--

LOCK TABLES `mission` WRITE;
/*!40000 ALTER TABLE `mission` DISABLE KEYS */;
/*!40000 ALTER TABLE `mission` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `tedata_or_te`
--

DROP TABLE IF EXISTS `tedata_or_te`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tedata_or_te` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tedata_or_te`
--

LOCK TABLES `tedata_or_te` WRITE;
/*!40000 ALTER TABLE `tedata_or_te` DISABLE KEYS */;
INSERT INTO `tedata_or_te` VALUES (1,'TE'),(2,'TEData');
/*!40000 ALTER TABLE `tedata_or_te` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacation`
--

DROP TABLE IF EXISTS `vacation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacation`
--

LOCK TABLES `vacation` WRITE;
/*!40000 ALTER TABLE `vacation` DISABLE KEYS */;
INSERT INTO `vacation` VALUES (1,'Sick Leave'),(2,'Unplanned Vacation'),(3,'Planned Vacation'),(4,'Call No Show'),(5,'No Call No Show'),(6,'Excuse'),(7,'Deduction');
/*!40000 ALTER TABLE `vacation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-22 18:59:48
