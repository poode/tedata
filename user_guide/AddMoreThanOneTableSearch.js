// every time a filter is applied, this is is invoked for every row in the table
var check_all_input_values = function(oSettings, aData, iDataIndex) {

        // check to see if this row matches all three of the filters. if any one
        // of them fails, return false to exclude it from results

        // compare the value from the platform search box to the
        // value in the platform column of this row (index 0 == platform column)
        // if it DOES NOT match the search term, exclude the result
        var platformSearchValue = $("#platform_search").val();
        var thisRowsPlatformValue = aData[0];
        if (!(thisRowsPlatformValue.match(platformSearchValue)) {
                return false;
            }

            // compare the value from the grade search box to the
            // value in the grade column of this row (index 1 == grade column)
            // if it DOES NOT match the search term, exclude the result
            var gradeSearchValue = $("#grade_search").val();
            var thisRowsGradeValue = aData[1];
            if (!(thisRowsGradeValue.match(gradeSearchValue)) {
                    return false;
                }

                // compare the value from the browser search box to the
                // value in the browser column of this row (index 2 == browser column)
                // if it DOES NOT match the search term, exclude the result
                var browserSearchValue = $("#browser_search").val();
                var thisRowsBrowserValue = aData[2];
                if (!(thisRowsBrowserValue.match(browserSearchValue)) {
                        return false;
                    }

                    // all three filters passed, so include this row!
                    return true;
                };

                // add custom filtering logic!
                $.fn.dataTableExt.afnFiltering.push(check_all_input_values);

                var myTable = $("#table").dataTable({
                    aoColumns: [{
                            sName: "Platform"
                        }, // "Platform" column is index 0...
                        {
                            sName: "CSS_Grade"
                        }, // "CSS Grade" column is index 1...
                        {
                            sName: "Browser"
                        } // "Browser" column is index 2...
                    ]
                });


                //Add listeners to the input box. Any time someone starts typing in the boxes, draw the table again. This will cause our custom filtering function to be applied.

                // every time someone types in any one of the input boxes, let's
                // trigger the custom filter function
                $("#platform_search,#grade_search,#browser_search").on("keyup", function() {
                    myTable.fnDraw(); // redraw the table, which will automatically invoke our filter again
                });
                ///////////////////////////////////////////////////////////////////////////////////////
                ////this if I want to add more than one search in a certain table
