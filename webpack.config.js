const webpack = require('webpack'), glob = require('glob');

let config = {

  entry: {
    'myPages': glob.sync('./bower_components/**/*.min.js'),
  },

  output: {
    path: '/var/www/html/tedata/client/scripts',
    filename: 'bundle--[name].js'
  },
};

module.exports = config;